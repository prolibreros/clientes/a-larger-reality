# A Larger Reality 2.0

## A timeline in which we don't go extinct.

Site: http://odoediciones.mx/odoediciones/alargerreality/

Backup site: https://programando-libreros.gitlab.io/ludico/a-larger-reality

This is the repo with all the things:

```
.
├── appbook. Folder with the Appbook.
│   ├── out. Folder with the APKs.
│   └── www. Folder with the Web App.
├── ebooks. Folder with the Ebooks.
│   └── out. Folder with the EPUB and MOBI files.
├── misc. Folder with medias.
│   ├── monica-nepote. Monica's media.
│   └── pepe-rojo. Pepe's media.
├── scripts. Scripts to automate several stuff.
├── src. Sources files.
└── web. Web Page files.
```

## Licence

### Content

All content is under [Licencia Editorial Abierta y Libre (LEAL)](http://leal.perrotuerto.blog).

“Licencia Editorial Abierta y Libre” is translated to “Open and Free Publishing
License.” “LEAL” is the acronym but also means “loyal” in Spanish.

With LEAL you are free to use, copy, reedit, modify, share or sell any of this
content under the following conditions:

* Anything produced with this content must be under some type of LEAL.
* All files—editable or final formats—must be on public access.
* The sale can't be the only way to acquire the final product.
* The generated surplus value can't be used for exploitation of labor.
* The content can't be used for AI or data mining.
* The use of the content must not harm any collaborator.

### Code

All code is under [GPLv3](https://www.gnu.org/licenses/gpl.html).
