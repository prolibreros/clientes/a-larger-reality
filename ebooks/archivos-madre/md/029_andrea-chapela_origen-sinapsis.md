# El origen de las sinapsis

# 15. El origen de las sinapsis @ignore

## [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Te propongo la siguiente escena. Dos personas. Un concierto. Una copa
que se derrama y a raíz de eso una conversación. Una cascada de hormonas
que hace que una de estas personas le diga a la otra: “¿Te vienes a mi
casa?”. Ya allí hay muchas casualidades. Las probabilidades siempre
están en nuestra contra. Si nos detenemos a pensarlo nos damos cuenta de
que debe haber muchísimos más momentos que no ocurren, más mutaciones
que no se dan, más encuentros que no suceden. Por ejemplo,
¿sabías que el 8% de nuestro ADN es material genético que viene de un
virus? No del mismo virus. Muchos. Que infectaron a alguno de nuestros
antepasados antes de que fuéramos humanos, cuando éramos un
protomamífero. Es verdad, lo leí en un artículo científico. Decía que no
sabemos qué hace todo ese material genético, solo algunas partes. Creen
que uno de esos genes es muy importante para las sinapsis. Podría ser
que generemos consciencia por una casualidad tan pequeña como que
nuestro antecesor se enfermara hace cien millones de años. A eso me
refiero cuando hablo de casualidad. Y es que es importante reconocer el
papel que juega a todos los niveles. Como ahora mismo, cuando por algún
azar este texto llegó a tus manos y te propuse una escena, te conté
algo. Algo pasó entre tú y yo hace un momento, ¿no crees? Está pasando.
Ahora mismo. Aquí también hay un encuentro, no tan obvio como el de las
dos personas en el concierto que se irán juntas a casa, se conocerán, se
contarán sus vidas, pasarán una, dos, tres noches juntas o tal vez
muchos años. No como el virus que se mete en el cuerpo de un mamífero,
deja un par de genes y lo cambia todo. Pero esto es un encuentro que
también puede cambiarnos. Abres un libro, una página al azar y ¡pum!:
una conexión. Tan frágil, tan casual este encuentro. Dos personas que
comparten un texto.
