# Vaccine No. 5

# 14. Vaccine No. 5 @ignore

## By [Mario Acevedo](997b-semblances.xhtml#a28) {.centrado}

When the T196 virus hit, the deadly plague raced unimpeded from airport
to airport, city to city, home to home. Within weeks, millions died.
Then the plague mysteriously halted.

My team labored to unravel the secrets of this disease, and why it
stopped just before wiping us all out. Shortly afterwards I was summoned
to the Center for Planet Sustainability.

Adele Haddon, chief of the strategic resources committee asked me, “Dr.
Mendoza, what stopped the plague?”

“Our immune systems adjust and cap mortality at fifteen percent.”

“Would it be possible to mutate the T196 virus to overcome a person’s
immune system?”

I knew what she was getting at. “Certainly, but then our immune systems
will compensate once again. My research shows that mortality would
remain at fifteen percent for each viral strain.”

Haddon said, “You’ve read our summary. Globally, we’re almost at the
point of no return. Extreme population control is our only practical
option for reversing a long list of ills: social violence, resource
depletion, and destruction of the environment.”

I’d done my homework. “We could administer the mutated plague as a
vaccine. Four iterations of successive strains at six month intervals
would get us to above 52 percent.”

“Our goal is fifty percent.”

I considered this. “To achieve the target level might require a special
fifth ’vaccine’ ---actually a fatal substitute. Selectively administered
until you achieve fifty percent.”

∗∗∗ {.centrado .espacio-arriba1}

I lost no sleep over my part in this conspiracy. After all, we were
saving the planet. Twenty-four months later I was invited to brief
Haddon about our ongoing success. But upon my arrival, guards dragged me
to a laboratory. Haddon and a medical tech were waiting. {.espacio-arriba1 .sin-sangria}

The guards held me down.

“It was a monstrous assassination you committed,” Haddon said.

“Me?” I shrieked. “It was your idea!”

“True,” she replied.

The tech readied a syringe labeled _Vaccine No. 5_.

“And the first rule of any assassination,” Haddon said, “is to do
justice to the assassins. Besides, we’re not yet at fifty percent.”
