# KIC 5520878 with variations and fugue

# 18. KIC 5520878 with variations and fugue @ignore

## By [Martha Riva Palacio Obón](997b-semblances.xhtml#a29) [interchangeable with author 1] {.centrado}

The movement of a pendulum [interchangeable with X’s memory] tends to
follow a pattern that includes the subset of all its possible behaviors.
The displacements ---infinite loops--- of a chaotic system through its
attractors are known as fractals. We can’t predict how the flap of the
butterfly’s wings [interchangeable with reminiscence 1] becomes a
hurricane [interchangeable with reminiscence 2], because we ignore the
initial conditions. The pulse of a star may conceal a second variation
of unknown origin. It’s possible that the ratio of two frequencies is
1.618 and it’s possible that in the precise moment that I break my arms,
a physicist in Hawaii [interchangeable with John Learned] connects the
rhythm of KIC 5520878 with the golden mean. Laplacian growth, bilateral
radial head fracture: the initial conditions that result into the story
of my life begin long before I’m born [interchangeable with strange
variable]. Golden mean, cosmic anatomy. I prefer Leonardo’s Saint John
the Baptist than the Mona Lisa [both paintings are interchangeable with
the blue flip-flops I wore the day I saw them at the Louvre]. To ride a
bike, the spiral of a nautilus [interchangeable with ammonite]. My
fractures are also my mother’s broken wrist and spending the day
[interchangeable with all the summers of my childhood] at the pool.
The calcium that flows back to the sea and my grandmother’s hip
[interchangeable with (void)]. Inside _Bach’s Contrapunctus No. 17b:
Inversus_ are all the fugues that will compose the last version of the
algorithm that now takes my dictation [interchangeable with author 2].
My broken arms move differently, they are desynchronized with the rest
of my body. Pendular swinging of higher extremities, return to a hominid
past [interchangeable with projection into a post-human future]. The
movement of a pendulum [interchangeable with X’s inverse memory] tends
to follow a pattern that includes the subset of all its possible
behaviors.
