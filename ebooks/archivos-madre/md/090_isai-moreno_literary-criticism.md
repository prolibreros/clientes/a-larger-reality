# Speculative Literary Criticism

# 45. Speculative Literary Criticism @ignore

## By [Isaí Moreno](997b-semblances.xhtml#a21) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

All narrative writing comes from an alteration in space-time that,
necessarily, detonates actions, a dramatic curve with ascents and
descents that is, a wave whose propagation is displaced by the ether
of thought. Einstein predicted the existence of waves that travel at the
speed of light from a crucial event between highly massive bodies. A man
writes with his pen. Next, a millimeter analysis tool is created,
adjusted to detect the shape of the dramatic wave in the story. The wave
doesn’t appear in the readings of the textual sensors: there’s no
dramatic curve. The man returns to the paper. He picks up the pen. He
scratches off everything and starts over on a new blank sheet. He writes
from his guts. He writes, fully aware he’s creating chaos. And it’s the
chaos that makes his narrative explode. Oppositions. Conflict. With a
simple graph, suddenly the dramatic wave appears in the writing,
undulating in space and breaking the barriers of time. Chaos has
materialized the miracle of a story that works. The chaos has been like
a massive body in collision whose gravitational laws conform nodes,
massive points of no return in an unsuspected narrative space, chaos
worthy of study for literary theorists.

Although in the mathematical exposition of his theories there
was also a narrative germ, Einstein didn’t suspect the existence of a
dramatic wave in writing.
