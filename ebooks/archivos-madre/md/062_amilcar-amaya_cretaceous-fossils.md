# Cretaceous fossils, images of an extinction

# 31. Cretaceous fossils, images of an extinction @ignore

## By [Amílcar Amaya](997b-semblances.xhtml#a03) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

Almost two hundred years ago, Joseph Nicéphore Niépce captured the first
images of what we may call photographs, he took them from his house
windows and called them “Point of view”. Currently is a little
complicated to see what’s printed in those old plaques, even though it
has only been two hundred years.

There is another kind of photographs, but they are more difficult to
interpret, and they’re what we call fossils. They can be very lasting, I
mean, several are conserved as rocks, but if we don’t know the context
in which the preserved was developing, they are not that useful. They
are inherently important because they allow us to recreate the world as
it was thousands or millions of years ago, although it is always better
when you can assemble those pieces with what goes up, down or to the
sides. Regularly, that global context is something that we lack when
talking of the fossil registry.

Some months ago (April 2019), Robert A. DePalma, researcher on the
Geology Department of Kansas University, published a first article where
he presents something that, if corroborated, would be one of the
greatest discoveries in modern paleontology.

Is difficult to find in the paleontology community someone that does not
recognizes the strength of the meteorite impact theory in the limit K/Pg
(Cretaceous-Paleogene). It is estimated that between 70 and 80% of the
species of living beings were affected or disappeared during that event.
What is constantly challenged is the reach of the event’s effect over
the extinction of the dinosaurs, a charismatic class used by DePalma to
present his and his team’s discovery to the world.

Other evidence points to the Earth going through a highly turbulent
time, with the continent that made up Pangea separating, an[]{#anchor}d
increased volcanic activity… in summary, it is possible that the
meteor _per se_ was not the direct cause of extinction, but, maybe, the
straw that broke the camel’s back.

Although there’s plenty of evidence of the impact of a comet, like the
crater in Chicxulub and sediment layers with iridium traces (rare
elements in the Earth but common in meteorites and asteroids) around the
world, there has always been interest in finding something more…
substantial; some clue that would explain to us what happened if not at
the moment of impact, at least during the subsequent hours, and be able
to see the first consequences of such catastrophic phenomenon. It has
been thought that the impact would have generated kilometer high tsunami
waves near the impact zone. It sounds incredible, but in that moment
incredible made all the sense in the world.

We must remember that seventy-five million years ago, a great inland sea
covered the center of what today is the United States, and a giant wave
could have easily covered that distance.

What DePalma has placed on the researcher’s table in his article is that
in a North Dakota town called Tanis, the seismic waves, that travel much
faster and to a greater distance than the aquatics, transformed the
environment of the region with violent floods, barely hours before the
tsunami landed.

Even for the geologists and paleontologists the geological period is
hard to assimilate. The climatic or ecologic effect of transforming
events such as a massive extinction, are only visible as a set of
changes throughout millions of years. Is like if we took a photograph of
a landscape with a very low resolution camera: we can guess the shape of
the mountains at the background, the grass color and maybe if there was
a person walking by that field at the moment the photo was taken, but
nothing more detailed than that.

Following that analogy, DePalma and his team of collaborators would have
taken a little piece of those blurry sections to convert it into a high
definition image. They describe a very energetic environment: choppy
waters, a mix of plants and animals stemming of a vast variety of
environments. The assumption is that the earthquakes caused by the
impact in Chicxulub, more than three thousand kilometers long, caused
important earth risings that, consequently, would push big masses of
water up, flooding large coastal areas, filling them with sediment and
waste. The great quantity of the suspended sediment would cover quickly
the remains, allowing their preservation.

But, like Carl Sagan used to say: extraordinary affirmations require
extraordinary evidence.

While the work presented by DePalma at first seemed robust, it has
caused some doubts due to the circumstances of where the announcement
was made. The team in charge of the discovery is aware that, if there is
something that draws attention about the cretaceous period is the
dinosaurs. And the article, that contains very important information,
barely mentions it. That is no problem, the past of life on Earth is so
much more than just dinosaurs, but DePalma announced in the New Yorker,
a publication not very related with the scientific press, the discovery
of multiple dinosaur’s fossils, meanwhile only one dinosaur discovery
it’s barely mentioned in the attachment of the main scientific article.

DePalma’s work has the potential of prividing the first detailed
photographs of a distant period in the history of life, a very
abbreviated time. A patient and detailed work would throw many
interesting data about geological and ecological alterations in such
particularly conditions. But the search of sensationalism, even with the
best of intentions, carries no advantage, just skepticism and doubts.
It’s not that the science doesn’t work with skepticism, the advantage of
the science method is that when you propose something that would change
some paradigm, there will always be someone there to test your idea, be
it to refute it or, in the best case scenario, corroborate it. And all
that’s left is to celebrate.

## References:

* [A seismically induced onshore surge deposit at the KPg boundary, North Dakota](https://www.pnas.org/content/116/17/8190)

* [Astonishment, skepticism greet fossils claimed to record dinosaur-killing asteroid impact](https://www.sciencemag.org/news/2019/04/astonishment-skepticism-greet-fossils-claimed-record-dinosaur-killing-asteroid-impact) {.sin-sangria espacio-arriba1}

* [The Day the Dinosaurs Died](https://www.newyorker.com/magazine/2019/04/08/the-day-the-dinosaurs-died) {.sin-sangria espacio-arriba1}
