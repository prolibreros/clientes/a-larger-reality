# Lemniscata

# 34. Lemniscata @ignore

## [Nelly Geraldine García-Rosas](997a-semblanzas.xhtml#a33) {.centrado}

E L I S A. Escribo cada letra con el mismo cuidado con que limpio los
espejos del interferómetro. ELISA. Repaso cada línea y curva para
asegurarme de que puedo escribir tu nombre incluso sin una pluma,
incluso con los ojos cerrados. E-L-I-S-A. Memoria muscular, ondas
imperceptibles que se propagan desde mi cuerpo a la velocidad de la luz,
trazos sin tinta que escriben en la urdimbre del espacio-tiempo.

Hace mil millones de años chocaron dos agujeros negros de masa estelar.
Habían estado orbitándose por un tiempo, bailando, acercándose cada vez
más rápido, hasta que se fundieron de forma tan violenta que todo el
universo pudo sentirlo. “Por un momento se vieron como una lemniscata”,
me dijiste, “como si quisieran representar la infinitud por sí mismos.
Pero, en un instante, se volvieron un círculo gigante, un cero, nada”.
Me reí porque estabas pensando en dos dimensiones. En tu mente, esos
agujeros negros eran figuras planas en una hoja de papel. Pero no se
hicieron “nada”, ahora son un objeto más grande. No como nosotras. Me
pregunto si alguien, alguna vez, medirá cómo distorsionamos el
espacio-tiempo cuando chocamos a nuestro modo particular, Elisa, aquella
vez que fuimos infinitas por un instante.

A veces, cuando escribo tu nombre, me pongo a trazar la S una y otra
vez. Parece una onda, como la que ha estado apareciendo en el
espectrograma los últimos días: un _glitch_ con un patrón de eco.
Calculé la frecuencia de sus repeticiones y estoy segura de que una onda
más grande viene en camino.

¿Cuál es la longitud de onda de tu nombre, Elisa? ¿Por qué te fuiste?
Tengo la necesidad de decir tantas cosas, de gritar, de que alguien se
entere cuán sola estoy en esta estación. Por favor, escúchame. Quien
sea.

Me llevó algo de tiempo codificar estos datos en forma de una onda
no-métrica que pueda montarse en la gran ola que viene. Miro este enorme
océano y, de todas formas, voy a tirar una botellita con un mensaje que
viajará como parte del universo mismo. Ahora tu nombre es infinito,
ELI8A.
