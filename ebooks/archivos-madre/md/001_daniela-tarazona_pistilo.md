# Pistilo

# 1. Pistilo @ignore

## [Daniela Tarazona](997a-semblanzas.xhtml#a10) {.centrado}

Me enfermé de lo mismo que ellas. Ahora que acudimos al hospital nos
vuelven a atender por turnos. Nos saludamos formadas en la fila para
donar nuestra sangre. La pureza existe en nosotras. Nada igual se ha
visto antes. Iluminamos los senderos entre los escombros. La más alta
enseña sus pómulos en los que han nacido brotes verdes. Yo muestro mis
senos cubiertos de musgo cuando me lo piden. La menor ya se arrastra por
el suelo, reverdecida y fosforescente por completo. La paciente
primigenia ahora yace a un lado del campo de futbol, es una superficie
de pasto. No sentimos hambre ni dolor. Estamos siendo herbáceas y
perfectas, oxigenadas y lubricadas por el agua sucia que llueve.
Perdimos la articulación de los pulgares, pero ganamos a cambio los
pistilos carnosos que nos salen de la garganta, con ellos respiramos el
aire espeso de la calle y lo devolvemos pleno de esporas. Hemos decidido
conquistar los pulmones de cualquiera porque es hora de ser vegetales.
Sabemos que se han terminado las diferencias. Enfermaremos al mundo para
ser idénticas. Los hombres son una amenaza. La contundencia de este
acontecimiento biodegradable es grave y a la vez benéfico. Echamos
raíces en nuestro interior: los alveolos son ahora extensiones de
nuestros cuerpos ecológicos, abrevamos el agua de nuestros pulmones,
somos también composta. No permitiremos que existan hombres que nos
tomen de la mano nunca más. Me enfermé de lo mismo que ellas. En las
palmas de las manos tengo espinas. Dicen que nos arderá el moho que
crece en los párpados de las animalas que resisten. Ignoro si la
purificación que experimento me lleva hacia la libertad. Creo que no,
pero mi piel es vegetal. Huelo a naftalina, sin embargo, porque la
salubridad es cosa vieja, un objeto guardado en el armario. Los jugos
verdes que preparamos nos dan la fuerza precisa para evitar cualquier
contaminación. La enfermera me saca sangre y mira mi escote y se
contagia de inmediato.
