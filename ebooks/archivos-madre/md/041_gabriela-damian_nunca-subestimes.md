# Nunca subestimes a una mujer dormida

# 21. Nunca subestimes a una mujer dormida @ignore

## [Gabriela Damián Miravete](997a-semblanzas.xhtml#a16) {.centrado}

La población estaba preparada para la erupción del volcán Popocatépetl,
el guerrero, que llevaba más de cien años adornado por su columna de
vapor, exhalando su mal humor por lo bajo; se hacía el importante con la
ocasional pirotecnia, que no eran sino malabares en el aire con unas
cuantas rocas fundidas y un flujo de lava más bien ralito, expulsado con
el placentero propósito de asustar. Y funcionaba: asustaba a la gente de
cerca y de lejos.

Los preparativos llevaban décadas: las viviendas a las faldas del volcán
ya eran cáscaras vacías de fruta, los pueblos se mudaron creando una
nueva periferia, como las cuentas de un collar. El parque nacional hizo
un plan de contingencia para la fauna; estudiaron adónde volarían los
búhos, colibríes, pájaros carpinteros y zopilotes, por qué rutas huirían
los cacomixtles y tlacuaches y mapaches y linces y coyotes y armadillos
y venaditos, a qué ramas o tallos se aferrarían las vanessas y a dónde
querrían treparse los lagartos y esconderse las culebras y cascabeles.
No podíamos arriesgarnos a perderlos. Se construyeron aquí y allá
sofisticadas madrigueras, sótanos-estuche, refugios pequeños, postes
altísimos de iridio desde donde observarían, como el resto de la gente,
el paso del fuego y el aliento temible de los lahares.

Estábamos listos. Después de terremotos zarandeadores, fumarolas
encanijadas y magma escupido, la gente se guardó en sus nuevas cajitas
antisísmicas, y esperó, mirando desde sus pantallas y azoteas.

Pero nadie había pensado en que el volcán Iztaccíhuatl, la Mujer Dormida
que velaba el guerrero, pudiera despertar. Sus chimeneas se habían ido
llenando de magma, poco a poco. Tanto esperaban del Popo que la habían
ignorado a ella. Iztaccíhuatl madrugó: aún brillaban las estrellas
cuando la nieve se hizo a un lado como si, con una patada, hubiera hecho
a un lado las sábanas para descubrirse los pies. Las explosiones
refulgieron sobre el cielo púrpura, y los ríos de viscosa lava
rojanaranja reptaron despacio por las laderas cual dragones de puro
fuego. Las nubes de ceniza tejieron telarañas de relámpagos y
estruendos.

Por alguna razón, nuestro miedo cambió. Vimos la erupción no como un
infierno, sino como un milagro futuro. Iztaccíhuatl no quería asustar a
nadie, simplemente necesitaba crear nueva tierra. Y como todas las
mujeres que despiertan, transformó nuestro paisaje.
