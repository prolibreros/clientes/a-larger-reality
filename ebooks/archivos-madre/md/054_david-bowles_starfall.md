# Starfall

# 27. Starfall @ignore

## By [David Bowles](997b-semblances.xhtml#a12) {.centrado}

Qhirtlahik watched the star stream toward his village.

The priest claimed stars were the Guardians of the Dark, where Mother
Sun hid her face in shame for the People’s sins each night. Alone of all
her feathered children she had given them souls, yet they were
ungrateful. So the divine visions proclaimed.

Qhirtlahik doubted. It was his sin. He watched the skies at night,
scratching the stars’ paths into a wide sheet of bark with his talon.

They did a dance. More regular than the tides, than the passing of the
seasons, than the coming and going of rain and heat and cool.

A week ago, he had noticed a new one, growing larger each night, defying
the dance.

Now, even with the dawn claws of Mother Sun bloodying the horizon, the
star would not fade. It was nearly as big as Sister Moon.

Qhirtlahik could have sworn he saw flames flickering across its face.

Looking down the hill at his village, he felt his feathers ripple with
fear. His tongue flicked at the air, a nervous impulse.

As one of the Knowers, Qhirtlahik had traveled far from the People’s
cluster of villages.

No other thinkers, no other speakers existed beyond their little spar of
land, jutting into the Iridescent Sea.

_Perhaps_ (he thought) _I should take my friends and flee_.

This star might consume us all. There would be no People left. No sign
we were ever here. Our wattle and daub homes shattered. Reduced to ash
our wooden weapons, bone tools. The sigils we score into flexible bark
effaced forever.

And beyond? What survives? The lumbering monsters whose feathers mimic
our own? The smaller ones, desperate to fly? The burrowers, warm-blooded
and timid?

Mother Sun thrust her face into the sky.

The falling star outshone her.
