# El objeto más extraño en todo el espaciotiempo

# 41. El objeto más extraño en todo el espaciotiempo @ignore

## [José Luis Ramírez](997a-semblanzas.xhtml#a22) {.centrado}

Yo. Los fotones orbitan mi horizonte, debatiéndose entre escapar
centrifugados hacia el vacío en todas direcciones o dejarse caer en
cascada hasta ser parte de mí. Más allá, el plasma rueda tan veloz como
es posible, arrastrando el marco del espaciotiempo en el mismo sentido
que yo todo observo; hay un campo magnético a mi alrededor, provocado
por los electrones que se precipitan. Eso soy yo. La masa de todas esas
partículas, la suma de sus cargas electrostáticas, su momento angular.
Nada más. En mi interior no tiene importancia alguna el número y los
diferentes tipos de materia, el balance entre _quarks_ y _antiquarks_…
Todo cuanto se vierte en mí se hace uno conmigo.

Aquí el tiempo fluye en ambos sentidos, sí, es el espacio el que avanza
inexorable hacia un único punto, hacia mí, la singularidad. Incubado en
y sobre la superficie del cosmos. Soy el haz del abismo y como tal, es
menester que me pronuncie, clamar mi nombre que es infinito en el tiempo
y único en el espacio, dejar constancia de cuanto ha acontecido conmigo,
gritar: Yo soy lo que soy y hago mía la única verdad del cosmos. La
belleza infinita de la oscuridad perdida en la traducción de una
antipoesía, jeringonza, nada sino ruido aleatorio. La radiación de
Hawking visible a más de un parsec del punto que la genera,
consumiéndose en luz, masa y energía desde su origen hasta
desintegrarle.
