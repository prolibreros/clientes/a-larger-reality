# Transmisible

# 10. Transmisible @ignore

## [Julia Rios](997a-semblanzas.xhtml#a24) \
Traducido por [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Cuando llegan, no los oímos ni los vemos. Los buscadores de OVNIs hablan
de señales de radio y luces en el cielo. Los científicos analizan por
mucho tiempo los datos hasta que se percatan de que son solo una
distorsión causada por un horno de microondas en el laboratorio de las
antenas parabólicas. Nunca nos molestamos en buscar qué pasó realmente.

El año en que llegan, la temporada de gripa es particularmente mala. Las
vacunas no nos inmunizan porque no puede cubrir todas las cepas. Muchas
personas mueren. Esto nos deja desolados. Asustados. Romantizamos los
hechos. Se ruedan tres dramas históricos sobre la pandemia de influenza
de 1918, que son aclamados tanto por la crítica como el público. “My
Dick Can Heal Ya Girl” de Lil Jabby llega al primer lugar de los
_rankings_. Habla de cómo, gracias a su milagrosa potencia sexual, sus
espermas son capaces de curar la enfermedad. Lil Jabby muere de gripa,
contagiado por una de sus _fans_.

Algunos sobrevivimos, pero el virus permanece en nuestro sistema. Justo
cuando pensamos que ha desaparecido, regresa con más fuerza. No
entendemos que este sufrimiento es un crecimiento. Que el virus es
información. Que nuestros bebés y los bebés de nuestros bebés lo
recordarán.

No nos daremos cuenta de lo que sucedió hasta generaciones más tarde,
cuando hayamos evolucionado. Cuando nuestros científicos descubran que
este fue el primer contacto. Que nos colonizaron. Que nos asimilaron.
Que no podemos distinguir un _nosotros_ de un _ellos_ y la idea de
intentarlo nos horroriza. Celebramos la vida de Lil Jabby y de otros
como él. Nos lamentamos porque nunca se convirtieron en nosotros, aunque
siempre están con nosotros. Recordamos a nuestros padres y a nuestros
abuelos e incluso las vidas de todas las criaturas que vivieron hace
tanto tiempo que apenas se parecen a nosotros, excepto, nos percatamos
ahora, de que ellos son nosotros. Nosotros somos ellos. Compartir la
Tierra nos hace un macroorganismo. Pero también estamos conectados a
otros lugares, a otros planetas. Somos la distorsión de horno de
microondas en la antena parabólica. La llamada desde el interior de la
casa.
