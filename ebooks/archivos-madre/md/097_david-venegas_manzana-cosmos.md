# Una manzana en el Cosmos

# 49. Una manzana en el Cosmos @ignore

## [David Venegas](997a-semblanzas.xhtml#a40) {.centrado}

Una de las frutas más famosas de la historia es la manzana que, según se
cuenta, golpeó a Isaac Newton en la cabeza mientras descansaba a la
sombra de un árbol. Esta extraña musa supuestamente inspiró a Newton
para tratar de explicar la atracción que experimentan los objetos unos
por otros, misma que mantiene en órbita a los planetas, y que hace que
estas frutas caigan e interrumpan las siestas bajo los manzanos.

Con su Ley de la gravitación universal, Newton explicó la caída de los
objetos y el movimiento de los planetas que se conocían a finales del
siglo +++XVII+++, así que pasó a la posteridad como el científico que logró
darle sentido al funcionamiento del universo conocido de su tiempo, con
una teoría casi completa. Sin embargo, la parte que no logró comprender
fue la naturaleza y el origen de esa atracción que experimentaban los
cuerpos entre sí. Cómo es que los objetos producían lo que denominó
Fuerza de Gravedad fue para para él un misterio inexplicable hasta el
día de su muerte, y se mantuvo sin respuesta durante dos siglos.

A principios del siglo +++XX+++, Albert Einstein analizó el problema desde
otro punto de vista. A pesar de que los cálculos de Newton eran
correctos y describían algo conocido, el avance de la tecnología
permitió conocer el movimiento del cielo de manera más precisa y las
observaciones ya no coincidían con lo que describía a Ley de gravitación
universal. Ante este problema, Einstein se dio cuenta de que, para
entender la naturaleza de la atracción entre los cuerpos, había que
dejar de pensar que se producía por una fuerza que emanaba de una
manzana, de la Tierra o de cualquier otro objeto. En 1915, postuló la
Teoría de la relatividad general para sustituir la teoría de Newton; en
en esta nueva teoría, Einstein propone que el universo entero es como un
tejido en el que se hilan y entrelazan el espacio y el tiempo; y que la
gravedad no es una característica propia de los cuerpos, sino una
consecuencia de la interacción de estos con el tejido del universo.

Para simplificar la relatividad general, podemos pensar que el universo
es como una tela estirada muy tensa. Imaginemos luego que una bola de
boliche colocada en esta sábana es el Sol. La depresión que la bola
produce en la tela representa la distorsión que el Sol crea en el tejido
del espacio-tiempo. Ahora, si hacemos rodar una manzana por la misma
sábana, esta dará vueltas alrededor de la bola, atrapada en su
distorsión, hasta que pierda velocidad y caiga hacia el centro de la
misma. Lo mismo ocurre con los planetas alrededor del Sol, pero estos
mantienen su velocidad porque no están rodando sobre una sábana, sino
desplazándose en el vacío; así que se mantienen orbitando alrededor del
sol, atrapados en su distorsión.

Todos los objetos generan distorsiones en el tejido del Cosmos, a las
cuales se les llama campos gravitatorios. Entre más masivo sea un
objeto, más intenso será su campo y más objetos podrán quedar atrapados
en este.

Ya ha pasado más de un siglo desde la publicación de la teoría de la
relatividad y, como es de esperarse, la tecnología que nos permite
observar el universo también ha avanzado. De hecho, el desarrollo de un
tipo muy particular de detector permitió que en 2016 se corroborara una
de las predicciones más ambiciosas de Einstein. Según la relatividad
general, el tejido espacio-tiempo se distorsiona en presencia de la masa
de los cuerpos, y es además el medio por el que se pueden propagar ondas
gravitacionales cuando ocurren explosiones o colisiones, como si fueran
ondas en la superficie de un estanque. El detector LIGO se construyó
específicamente para detectar estas ondas y con él logramos observar en
2016, por primera vez, las perturbaciones en la estructura del universo
que produjeron dos agujeros negros al chocar entre sí.

Gracias a descubrimientos como éste sabemos que algunos aspectos del
universo funcionan como Einstein los predijo, aunque también hemos
observado fenómenos que nos sugieren que la teoría de la relatividad
podría tener algunos problemas.

Una de estas dificultades surgió cuando pudimos saber el valor de la
masa en conjunto de todas las estrellas de una galaxia, y determinar la
intensidad de su campo gravitatorio. De acuerdo con estos cálculos,
fundamentados en la relatividad general, las galaxias no podrían
mantener su forma solo con el campo gravitatorio de sus estrellas, pues
giran tan rápido que deberían salir disparadas y dispersarse por el
Espacio. Sin embargo, las estrellas se mantienen dando vueltas alrededor
del centro de las galaxias, formando conjuntos bien definidos. Si
queremos encontrar una explicación siguiendo al pie de la letra la
teoría de Einstein, debemos suponer que existe una gran cantidad de
materia además de las estrellas, que está aportando la masa necesaria
para que el campo gravitatorio de la galaxia sea suficientemente intenso
para mantener a las estrellas en su sitio. El problema es que esta
materia es indetectable por cualquiera de los medios que conocemos y,
por lo tanto, no tenemos manera de saber su naturaleza o identidad, así
que se le ha denominado materia oscura.

Otro problema es que, según los cálculos de la relatividad general, el
universo se debería estar expandiendo a una velocidad constante, pero
sucede que dicha expansión ocurre a un ritmo cada vez más acelerado. De
acuerdo con el trabajo de Einstein, la única manera de que esto ocurra
es que exista una fuerza de repulsión que acelera continuamente esa
expansión. Nuevamente, esta fuerza es indetectable, pues no hemos
logrado observarla y mucho menos estudiarla, así que se le da el nombre
de energía oscura.

Y así, casi por casualidad, nos encontramos de nuevo lidiando con un
problema parecido al que se enfrentó Newton hace más de 300 años.

Debido a la magnitud de su influencia, se calcula que la materia y la
energía oscuras representan en conjunto el 95% del universo; esto
equivale a decir que, si el Cosmos fuera una enciclopedia de 20
volúmenes, solo somos capaces de leer el primero, mientras que los otros
19 contienen el origen y la naturaleza de formas de materia y energía
que actualmente son un enigma para nosotros. También es posible que las
cuentas no nos cuadren porque necesitamos hacer otro cambio radical en
nuestra manera de entender el espacio-tiempo, así que hay científicos
trabajando en explicaciones alternativas como la teoría de cuerdas o la
posibilidad de los universos paralelos.

Mientras tanto, el Cosmos se sigue expandiendo, las galaxias y los
planetas siguen orbitando, y las manzanas siguen cayendo sin que sepamos
exactamente por qué.
