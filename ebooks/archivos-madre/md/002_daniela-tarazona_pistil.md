# Pistil

# 1. Pistil @ignore

## By [Daniela Tarazona](997b-semblances.xhtml#a10) \ 
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

The same thing that got them got me. Now that we’re in the hospital,
they attend to us in turn. We greet each other while waiting in line to
give blood. There’s purity in us. It’s something that has never been
seen before. We illuminate the paths through rubble. The tallest one
shows her cheekbones where green shoots have sprouted. I show my
moss-covered breasts when they ask me. The youngest one already crawls
on the ground, completely green and phosphorescent. The primal patient
now lies on one side of the soccer field that has a grass surface. We
don’t feel hunger or pain. We’re just herbaceous and perfect, oxygenated
and lubricated by the dirty rainwater. We lost the articulation of our
thumbs, but we gained in return the fleshy pistils that come out of our
throats with which we breathe the thick air of the street and fill it
with spores. We’ve decided to conquer anyone’s lungs because it’s time
to be vegetables. We know the differences are over. We’ll make the world
sick in order to be identical. Men are a threat. The strength of this
biodegradable event is serious and at the same time beneficial. We take
root in our interior: the alveoli are now extensions of our ecological
bodies. We drink the water from our lungs. We’re also compost. We’ll not
allow men to take us by the hand anymore. The same thing that got them
got me. Thorns grow in the palms of my hands. They say the mold that
grows on the eyelids of she-animals that resist will burn us. I don’t
know if the purification I go through leads me toward freedom. I don’t
think so, but my skin is vegetable. I smell naphthalene, however,
because healthiness is a thing of the past, an object stored in the
closet. The green juices we prepare give us the strength necessary to
repel any contaminants. When the nurse draws my blood and looks at my
cleavage, the virus spreads like wildfire.
