# En la orilla de la nada

# 39. En la orilla de la nada @ignore

## [Efraím Blanco](997a-semblanzas.xhtml#a14) {.centrado}

El científico miró otra vez la fórmula que lo resolvía todo y luego miró
otra vez hacia el infinito. Estaba, según sus cálculos, a la orilla de
la nada. Lo único que sostenía su humanidad era esa cuerda y la mesa que
había logrado equilibrar sobre ella. Encima de la mesa descansaba la
hoja de papel con la fórmula matemática que resolvía la teoría del
multiverso. Las simulaciones en el laboratorio eran bastantes claras: si
tensaba la cuerda, crearía más universos casi iguales al suyo; si
aflojaba un poco, algún universo casi idéntico sería destruido en otro
lado. Lo único que equilibraba la cuerda encima de esa nada era el
pequeñísimo dios que la sostenía del otro lado. Estaban tan lejos uno
del otro que apenas si alcanzaban a verse. Ni el dios ni el científico
estaban seguros de lo que ocurriría a continuación, pues si se fijaban
bien, aquella nada era una casa de espejos donde su imagen se reflejaba
sin fin en lo que parecía ser el ombligo de cada universo. Descansaban,
entonces, sobre la cuerda original que habría dado origen a todo.

Llevaban así un buen rato cuando un colibrí se paró encima de la cuerda.

Para otros no fue un ave, sino un dragón o una mosca, un delfín con
gafas o John Lennon con una guitarra desafinada. Todas las cuerdas, eso
sí, se sacudieron al mismo tiempo. Todos los dioses lloraron y el
científico entendió que había llegado el momento. Se acomodó la bata,
los lentes, miró otra vez la fórmula y luego asintió con un pequeño
movimiento de cabeza hacia todos los otros científicos encima de una
cuerda que asentían de regreso hacia él. Supo la verdad y miró por
última vez a los dioses, al tiempo que sacudía la cuerda con violencia,
como si quisiera librarse del colibrí que la tensaba, del dragón, de las
moscas, de todo lo que sobrara para terminar de crear en paz su propio
universo multiplicado por los tiempos de los tiempos.

El colibrí voló.
