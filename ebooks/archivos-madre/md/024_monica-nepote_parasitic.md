# Parasitic Language

# 12. Parasitic Language @ignore

## By [Mónica Nepote](997b-semblances.xhtml#a31) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

<script type="text/javascript" src="../js/poetry.js"></script>

[It’s](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[not](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[them](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[It](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[reproduces](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[itself,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[isolates](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[itself,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[returns,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[takes](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[form](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[dryness](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[inside,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[that](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fire,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[that](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[stone](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}

[It’s](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[not](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[them](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[I](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[have](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[to](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[think](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[about](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[the](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[thread,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[have](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[to](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[think](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[the](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[stone](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[of](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fire](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[has](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[a](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[fissure,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[a](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[drop](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[is](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divided](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplies](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[an](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[identical](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[soon](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[takes](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[form](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[and](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[then](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[one](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[divides](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[multiplies](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[with](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[with](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[with](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[another](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[less](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[and](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[less](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}


[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[my](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[body,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[I’m](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[its](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[body,](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[its](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
[planet](https://prolibreros.gitlab.io/cc/a-larger-reality/misc/monica-nepote/)
{.poesia}
