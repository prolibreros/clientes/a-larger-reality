# Viral ocean

# 19. Viral ocean @ignore

## By [Cisteil X. Pérez](997b-semblances.xhtml#a39) \
Translated by [Valeria Becerril](997b-semblances.xhtml#a54) {.centrado}

_An inefficient virus kills its host. A clever virus stays with it_.\
James Lovelock {.epigrafe}

It seems strange, that while being living beings, we cannot easily
differentiate between what’s alive and what’s inert material. But so it
happens, for decades, that the debate about the status between
“living/nonliving” of viruses and their non-classification as
“organisms” has remained an open one. What happens is that viruses are,
let’s say, pretty special. For example, they are considered as
“nonliving” because they have phases in which they are completely inert;
they are minuscule ---nearly 50 times smaller than many bacteria--- plus
they are not able to reproduce on their own, they require a host cell to
be able to multiply and inherit their genes. On the other side, whether
they reproduced, they have the capacity to produce their own genes and
to evolve, which marks them as living entities. These characteristics
are what continue to challenge the typical concepts that we use to
define the organisms and what keeps the debate open about the concept of
_life_ itself.

Recently, new findings seem to have tilted the scales in favor of the
living status of the viruses: it was discovered that the viral particles
in the natural ecosystem are far more diverse and abundant than it was
first thought, maybe even more than cells; it was also determined that
the virus that infect species of the main three branches that conform
the tree of life ---domains Bacteria (true bacteria), Eukarya
(protists, algae, plants, animals and fungi) and Archaea (similar to
bacteria, but extremophiles)--- have a close evolutionary relationship
among them, established thousands of millions years ago; and also giant
viruses have been detected, in other word, with size and genetic content
comparable to many bacteria cells. Then, in case of being living
entities, in which part of the tree of life would we have to add
viruses?

One of the main issues to solving this problem is that when we add the
different groups of viruses to the tree of life, these are scattered
among the branches that derive from the three domains, since there are
many shared genes between the viruses and cellular organisms. This is
partially explained through the way in which the viruses are multiplied:
they infect a cell, kidnap their genetic machinery and reprogram it to
make copies of their genome; in the process, they can steal genes of
their host cell and mixed them with their own in order to acquire a
greater cell infection capacity that their progeny will inherit. This
strategy is so successful that their hosts have developed many defense
tactics against viruses and, in many cases, it has been better to
stablish a mutualistic relationship with the virus rather than fighting
it.

Some of these relationships have been intimately linked during such a
lengthy period of time that the genetic material of the participant is
too intertwined and many mutualistic relationship viruses are essential
for the survival of the hosts. Such is the case of the interaction that
happens between viruses and parasitic wasps of the Brachonidae and
Ichneumonidae families. The parasitic wasps lay their eggs inside their
hosts (caterpillar, spiders, aphids, among others) so when the larvae
emerge, they are able to feed little by little of their host, while it
is still alive, until reaching the proper maturity to go outside,
reminiscing of ALIEN. However, the small detail is that this could not
happen without the aid of the virus that accompanies the eggs when their
mother inserts them. Since without it, the eggs would interpret their
new environment as a threat and would stop its development (Now that I
think about it, would it be possible that the xenomorph carried a virus
to infect their hosts?). A similar situation has occurred for millions
of years between viruses and mammals, in which a virus is totally
responsible for the development of a protective barrier that is embedded
in the placenta; that barrier is essential to avoid the body of the
mother identifying the fetus as an intruder and attempting to eliminate
it.

Viruses can also proportionate the host with a greater competitive
capacity in their habitats or to face their enemies. For example, the
virus that accompany some bacteria and yeasts are able to eliminate the
organisms that could compete or threat their teammates, via infection or
by weakening them through the liberation of toxins. Macroscopic
organisms such as invasive plants carry with them a virus that weakens
native species; and in the history of humanity there are different
episodes marked for invasions that were easier after the infection with
viral diseases such as influenza, measles and chickenpox. In the last
few years, viruses that attack bacteria resistant to antibiotics and
viruses that attack other viruses, such as the hepatitis G virus that
suppress the superinfection with +++HIV+++, has begun to be used. In other
cases, viruses confer their guests a greater resistance to changes in
the environment through the transferring of genes that allows their
hosts a quick adaptation and the development of new functions, such as
the ones that give them a greater tolerance to droughts and heat.
Obviously, we are already trying to exploit this capacity to the fullest
extent on the harvest of tomatoes and other foods in extreme
environments.

This kind of relationships between viruses and their hosts, along with
other associated to the development of defense strategies against
viruses, why it is considered that these had an important role in the
evolution of life on Earth and that they constituted a factor that
notably influenced the evolutionary changes of the organisms with which
these entities interact. So, if we discarded viruses when we tried to
rebuild the tree of life, we would be left with some gaps and incomplete
branches due to the lack of information that they themselves provide.

All these interactions between viruses and the three domains of life
have merged little by little through hundreds of millions of years and
have created networks so intricate and complex that we are not even
close to fully understand them. However, we can start by accepting that
the viruses are not a branch of the tree of life, they actually are the
whole tree, it lives and grows, from its roots, immersed in a viral
ocean.
