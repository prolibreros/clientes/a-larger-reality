# Sueño bacterial

# 13. Sueño bacterial @ignore

## [Cecilia Eudave](997a-semblanzas.xhtml#a09) {.centrado}

Soñé que me soñaban, me han dicho que es el principio del fin. Desperté
sudando, envuelta en una nebulosa caliente, la respiración agitada, la
vista aturdida aún con restos de mis sueños confundidos en los suyos.
Estoy segura, no es una infección cualquiera, las bacterias van ganando
terreno. Cuando me enteré de que hacían transferencias horizontales a mi
ADN para confundirlo y resistir, entré en paranoia. ¿Cómo lo hacen?, es
difícil de creer: ¡son peludas! Arponean mi pobre ácido
desoxirribonucleico con vellos tan delicados, tan resistentes como el
hilo de Ariadna. Imaginé mi cuerpo entrecruzado por pelos formando una
red intrincada de conexiones increíblemente sensibles alrededor de mi
cerebro, creando un enorme capullo en donde anidar sus pesadillas y
controlarme. Terrible. Por eso me inscribí en un grupo de apoyo que sabe
cómo frustrar bacterias. Algunos de sus miembros llevan años probando
todo tipo de antibióticos y tratamientos experimentales. Les conté mi
caso, no podían dar crédito a lo que las ingratas estaban consumiendo.
“¿Quién eres sin tus sueños? Son unas perversas, ve sin piedad tras
ellas”. Me recomendaron la fagoterapia: bacterias come bacterias.

¿Funcionará? Las que me habitan son muy fieras, están decididas a
infectarme con sus recuerdos milenarios hasta asimilarme. Parecen un
pueblo repudiado, maldito, reproducen sus malignos modos, sus conductas
nefandas hasta el infinito para seguir persistiendo. Saben refugiarse en
el dormir o en el delirio, de los seres vivos su más primitiva y
poderosa esencia vital, así van de cuerpo en cuerpo, se hacen eternas.
No sé qué vieron en mí y no en otro, posiblemente les gusta cómo sueño,
o les gusta cómo me sueñan. Resulté un buen caldo de cultivo.

Tal vez no sea tan malo si pierdo esta guerra, quizá perviva algo de mi
existencia en una pequeña escena onírica recluida y repartida entre
todas ellas. O tal vez, nos quieren recordar que nosotros somos a su vez
bacterias dentro de otro organismo agonizante, siempre con hambre
egocéntrica, siempre soñando que alguien más nos sueña.
