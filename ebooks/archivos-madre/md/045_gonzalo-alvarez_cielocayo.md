![](../img/gonzalo-alvarez_skyfell-cielocayo01.jpg)

![](../img/gonzalo-alvarez_skyfell-cielocayo02.jpg)

# _Cuando se cayó el cielo_ de [Gonzalo Álvarez](997a-semblanzas.xhtml#a47) {.centrado}

El tema que dio lugar a esta secuencia fue el Cretáceo-Paleógeno, particularmente la extinción. Un artículo que leí mencionaba que los dinosaurios podrían haberse extinguido no por un meteorito, sino por una erupción volcánica masiva, 250 000 años antes de la fecha que fija la teoría actual del meteorito. Las trampas Deccan son depósitos de lava enfriada del tamaño de una montaña en la India, y algunos creen fue la causa de la extinción, por lo que quise crear una imagen basada en esa teoría. Además, cuando estaba haciendo el trabajo, traté de ponerme en el lugar de esas criaturas que vivían en este planeta mucho antes que yo y pensar en cómo debieron experimentar este evento. Seguramente miedo y dolor fue lo que sintieron y traté de ilustrar esa angustia en la segunda imagen, mientras que la primera imagen se enfoca más en establecer el evento.

# [Gonzalo Álvarez](997b-semblances.xhtml#a47), _When the Sky Fell_ {.centrado}

# 23. Cuando se cayó el cielo / When the Sky Fell @ignore

The theme that moved me to this sequence was the Cretaceous-Paleogene and particularly the extinction. An article I read mentioned that the dinosaurs could have in fact been killed not by a meteor, but by a massive volcanic eruption 250,000 years before the current theory of the meteor. The Deccan Traps are mountain-sized cooled lava in India that is said to have been what caused the extinction, and so I wanted to create an image based on that theory. Also as I was creating the work I tried to put myself in the shoes of these creatures that lived on this planet way before me and think of how they must of experienced this event. Surely fear and pain was something they felt and I tried to illustrate that with the second image while having the first image focusing more on establishing the event. 
