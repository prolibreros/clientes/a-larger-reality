# ¿Eureka?

# 47. ¿Eureka? @ignore

## [Blanca Mart](997a-semblanzas.xhtml#a08) {.centrado}

Albert aprovechó un descuido del científico loco y entró en su máquina
del tiempo. Sabía que no debía hacer eso. No se entra en una máquina del
tiempo así como así y menos si es de un colega, y menos si es de un
colega loco. Pero él quería conocer a Arquímedes y decir “eureka”. Pulsó
un botón y apareció en una amplia casa en Siracusa.

Allí estaba el matemático, en pleno baño, pensando mientras se bañaba,
calibrando, calculando. De pronto gritó “¡Eureka!” y salió corriendo.
Albert estudió precipitadamente sus papeles y, pleno de ciencia, pulsó
el duplicador que se había traído de su propio tiempo. De nuevo acertó y
regresó al siglo +++XX+++.

Pero no sabía en qué fecha ni en qué lugar estaba. ¡Dichosa máquina!
¿Qué era aquello?, ¿una variable…? Parecían unos estudios de cine. Se
abrió una puerta y una hermosísima mujer vestida de negro apareció
frente a él, canturreaba “Put the Blame on Mame”, una seductora canción,
mientras movía levemente su larga y rizada cabellera y se quitaba
lentamente los largos guantes.

Le miró con curiosidad.

---¿Quién eres?

---Me llamo Einstein.

Ella se acercó mucho y le sonrió.

---Yo, Gilda, en ese filme, ¿sabes…?@note

Él susurró: "Eureka". Más tarde pulsó el mismo botón casi sin darse
cuenta y apareció en su propio laboratorio.

“¡Eureka!”, repitió, el cabello electrocutado, de punta.

¡Menudo viaje en el tiempo!

Por desdicha, al gran estudioso, se le olvidaron las matemáticas.

Son cosas que ocurren si uno entra en una máquina del tiempo. Porque no
solo es el tiempo, sino los versos que cantan entre las cuerdas. {.espacio-arriba1 .sin-sangria}
