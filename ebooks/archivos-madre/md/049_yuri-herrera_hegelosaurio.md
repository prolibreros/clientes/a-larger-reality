# El Hegelosaurio

# 25. El Hegelosaurio @ignore

## [Yuri Herrera](997a-semblanzas.xhtml#a37) {.centrado}

Esto que usted ve es la prueba, única pero incontrovertible, de la
racionalidad dinosáurica y, por desgracia, también de sus últimos
momentos sobre la tierra. Al contrario de lo que se ha repetido desde
siempre, los dinosaurios eran sensibles, juiciosos, y reflexionaban. Lo
cual no debería sorprendernos, considerando que no tuvieron mucho qué
hacer durante millones de años. Nomás no tenían prisa. Aunque deberían
haberla tenido, porque para cuando llegó la gran extinción los pobres ya
eran demasiado grandes y la comida de la que disponían ya no abundaba y
poco a poco habían comenzado a declinar. Bueno, pues había dos maneras
en las que estos animalitos procesaban sus ideas: una, dejando que el
mundo les interesara el cuerpo poco a poco, que la lluvia les macerara
la piel gruesa y rugosa, que el viento les indicara algo sobre las
constantes de la naturaleza, que los bichos más pequeños anidaran en su
epidermis. Así iban entendiendo cosas. No como las enseñamos ahora, en
párrafos, sino como una especie de fluido entre su organismo y los otros
organismos. La segunda era, cómo decir, su ciencia dura, la trituración
del mundo hasta hacerlo digerible, es decir, masticando. Los dientes
eran los engranes de su racionalidad. Gracias a estos dos mecanismos los
dinosaurios desarrollaron una filosofía que probablemente hubieran
llegado a poner por escrito de no ser por el meteorito que terminó de
destruir lo que los volcanes y la evolución ya habían comenzado. Pero no
lo hicieron… ¿O lo hicieron?… _Lo hicieron_. En particular un
herbívoro. Recientes investigaciones han mostrado que esto que ve es un
vestigio de lo que uno de los herbívoros, a quien he llamado
Hegelosaurio (disculpe, no pude resistirme), emprendió cuando se dio
cuenta de que esa luz que crecía en el cielo era la catástrofe. No puedo
reconstruir la cosmovisión entera que este individuo plasmó en esas
¿horas? que tuvo antes de que todo se fuera al carajo; es como si
contáramos solo con una página de la _Fenomenología del espíritu_. Pero
sí le puedo decir que es una página hermosa, que la masticación ahí
eternizada plasmó una manera de entender el paso del tiempo y el temor
ante el cataclismo. Sigo descifrándola, pero ya puede verse ahí, vea,
ahí y ahí, esos no son accidentes, son matices. Qué. No. No. No. Se
equivoca. Esto no es una piedra.
