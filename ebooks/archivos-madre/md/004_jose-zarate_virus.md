# Virus

# 2. Virus @ignore

## By [José Luis Zárate Herrera](997b-semblances.xhtml#a23) \
Translated by [Toshiya Kamei](997b-semblances.xhtml#a53) {.centrado}

Cartomancy {.espacios .sin-sangria}

“Well… What’s in my future?” {.sin-sangria}

The fortune-teller looked at the cards in horror. There was no Death,
the Moon, the Fool, or the Lovers. She had never seen the ones spread
out before her: the Kaiju, the Asteroid, the Virus, the Supernova… {.sangria}

Report {.espacios .sin-sangria}

Virus apparently not pathogenic, but armed. {.sin-sangria}

Origin {.espacios .sin-sangria}

“Planet Earth,” the guide pointed out, “the original home of those who
conquered the universe. The viruses were born in that insignificant
world.” {.sin-sangria}

Influence {.espacios .sin-sangria}

Hardly anyone knows that viruses become worse under the full moon. {.sin-sangria}

Pandemic {.espacios .sin-sangria}

The spontaneous combustion virus made, for the last time, the countless
windows in the city shine. {.sin-sangria}

…We Have a Problem {.espacios .sin-sangria}

“The organic ecosystem we are part of has also been influenced by
radiation, zero gravity, and the extreme conditions of outer space.
Something transmits from within us, Houston, and they tell us they’re
going to seize control, tyrannical oppression is over, and the bacterial
population will finally be free from our horrible bodies.” {.sin-sangria}

Autoimmune Disease {.espacios .sin-sangria}

It healed only when it managed to completely remove itself. {.sin-sangria}

Degrees {.espacios .sin-sangria}

Recent research has found that the so-called Z virus, responsible for
the zombie infestation during the last decade, dies above fifty degrees
(Celsius). {.sin-sangria}

If you want to continue using your living undead (as pets, labor, fun)
without risking infection, just boil them. {.sangria}

Medical Mystery {.espacios .sin-sangria}

The virus kills only those whose middle name begins with G. {.sin-sangria}

Da Vinci {.espacios .sin-sangria}

Thousands of scientists have marveled at Leonardo Da Vinci’s detailed
anatomical sketches. They are so accurate that, not surprisingly, no one
has discovered his one-to-one scale drawings of viruses. {.sin-sangria}

Window {.espacios .sin-sangria}

The electron microscope results trembled in his hand. {.sin-sangria}

His face was a white moon when he told me, “The shadows---the virus is transmitted through the shadows.” {.sangria}

As we looked out the window, the night engulfed the world outside the
laboratory. {.sangria}
