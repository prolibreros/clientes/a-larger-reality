# Hunger

# 5. Hunger @ignore

## By [Andrea Ciria](997b-semblances.xhtml#a06) {.centrado}

The last green branch of the shrub began to dry up and then adopted the
yellow tonality of the rest of the trees. Silence was everywhere. Dead
ants could not devour the corpses of fallen carrion eating birds, which
in turn could not eat the bodies of other animals that made up an
infinite mat of inert bodies. For the first time in history, all species
on Earth, including human beings, seemed to be together, in a static and
lifeless harmony.

The reckless Borax-1 virus easily modified the genetic information of
the cells of living beings to multiply itself and swallowed up all the
living particles of the planet. When all began, the scientists around
the world tried in vain to fight the pandemic. But there was no way to
prevent the cells from becoming viruses, neither alive nor dead, which
were always ready for a new host. Experienced researchers assured, and
they were not far from the truth, that the Borax-1 was superior to any
other known virus because it could travel by air and under water, and it
would turn the Earth into a barren place. Other experts doubted about
the superiority of the virus because besides that it could easily mutate
to stay in any type of cell, it was also unable to stop mutating, and it
would soon ran out of hosts. However, what everyone agreed on was that
the Borax-1 came from space.

When they were certain of the death of the last cockroach,
the most resistant without doubt, the creators of the Borax-1 came out
of the depths of the sea. They were hungry. Little by little they
settled in the houses of the deceased. Soon after, small fires could be
sighted everywhere, in which they burned the corpses. It was time to
eat. The porcelain plates were full of ash.
