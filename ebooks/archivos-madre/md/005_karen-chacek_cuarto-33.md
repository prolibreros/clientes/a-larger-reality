# Cuarto 33

# 3. Cuarto 33 @ignore

## [Karen Chacek](997a-semblanzas.xhtml#a25) {.centrado}

Al anciano sin dientes, atornillado al banquillo de la recepción, le
aclaro: “En circunstancias normales, yo nunca me hospedaría con cuatro
palmas bambú y dos hiedras”, en una habitación sin ventanas como la que
le urgí que me asignara. Siempre me han parecido deprimentes las
recámaras con cortinas de _jacquard_ que simulan ventanales donde hay
paredes de ladrillo o que esconden viejos calefactores que el esplendor
desconoció al partir. Pero hoy no es un día de circunstancias normales:
quien mire hacia la calle enfermará de pura sugestión. En la tele
aconsejaron encerrarse en un cuarto con al menos seis plantas para
purificar el aire y clausurar las ventanas: “Volvernos invisibles, en lo
que el peligro pasa”. Yo solo venía a un simposio sobre lenguas
extintas; ahora estoy varada en el núcleo de una contingencia de
magnitud incalculable, bañada en sudor amarillo de pies a cabeza.

La voz del locutor de noticias me provoca náuseas; traduce al momento el
comunicado que alguien más lee en un inglés golpeado; repite que los
potenciales beneficios para la salud pública siempre han sido mayores
que los riesgos, que haber levantado en 2017 la prohibición de crear
virus letales en laboratorios fue un acto responsable: “La naturaleza es
la mayor bioterrorista y tenemos que hacer todo lo posible por estar un
paso por delante”; cita íntegra de la declaración del presidente de la
Junta Nacional de Asesoramiento Científico de Bioseguridad del mismo
país que construyera una trinidad de deslumbrantes laboratorios, en esta
pequeña ciudad latinoamericana olvidada por el dios de los sistemas
satelitales, con una temperatura promedio de 37 grados centígrados y
variedad de artefactos de aire acondicionado en desuso repartidos por
sus viejos edificios.

El anciano de la recepción sonríe como si morir le diera igual. Me
entrega la llave del cuarto 33.

Traslado las plantas en un mohoso carro portaequipaje; al final del
pasillo, la mano me tiembla frente a la puerta: “Tranquila, cariño ---me
digo---. A la edad de seis años podías desaparecer del mundo con solo
enrollarte en una cortina. ¿Por qué no habría de funcionar eso ahora?”.
