# Luz de estrella

# 33. Luz de estrella @ignore

## [Angela Lujan](997a-semblanzas.xhtml#a07) \
Traducido por [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Abigail no se mueve mientras observa el cielo nocturno por la ventana.
Coloca una mano sobre la superficie fría del cristal. Ojalá pudiera ver
más que ese puñado de estrellas, cuya luz apenas es suficiente para
atravesar la contaminación lumínica de la ciudad.

Esa ciudad se está alejando cada vez más. Su hogar se está alejando cada
vez más. El bamboleo del elevador la obliga a apoyarse en la ventana.
Por un momento, teme que el cristal se rompa y ella caiga hacia la
Tierra. Pero se mantiene, sólido, frío bajo sus dedos. Cuando se vuelve
a sentir segura, camina de regreso a su asiento.

El elevador es grande. Hay otras personas sentadas sin orden, pero
apiñadas junto a las ventanas. Como Abigail, la mayoría viajan solas.
Ella está segura de que extrañarán la Tierra, pero nadie los extrañará a
ellos.

Abigail siempre había buscado silencio y soledad. Pero ya comienza a
echar de menos la comodidad del hogar. Quería alejarse del sonido, del
caos y las luces falsas de la ciudad, encontrar una estabilidad apacible
y contemplar, por fin, la luz real de las estrellas.

En soledad, el silencio le atrae menos. Anhela el ruido y distracción
urbanos. Le servían de contraste. La exponían. Aquí, en este silencio,
se siente una extensión de su entorno. Sólo más silencio. Observa la
oscuridad entre las estrellas. Sabe que alguna vez estuvieron más cerca,
pero que una fuerza en el universo las está alejando. Lo aleja todo cada
vez más. Imagina una mano que empuja las galaxias como si fueran migas
de pan. La imagina acunando el elevador, llevándola lejos de la ciudad,
de la Tierra, hacia otro planeta y otro universo.

Se estremece y aparta ese pensamiento. Mira de nuevo por la ventana.
Allá abajo, la ciudad es solo un punto de luz. Desde aquí las estrellas
se ven más brillantes. Más abundantes. Las mira moverse por el
firmamento. Sonríe. Ha encontrado por fin luz de estrella. Su viaje
apenas comienza.
