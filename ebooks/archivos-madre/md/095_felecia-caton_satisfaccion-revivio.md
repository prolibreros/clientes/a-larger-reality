# La satisfacción lo revivió

# 48. La satisfacción lo revivió @ignore

## [Felecia Caton Garcia](997a-semblanzas.xhtml#a15) \
Traducido por [Andrea Chapela](997a-semblanzas.xhtml#a05) {.centrado}

Sebastian se levantó de su escritorio y lanzó el fajo de papeles al otro
lado de la habitación.

---¡Ya está! ---gritó en dirección del otro ocupante de la habitación:
una gata de pelaje negro y brillante que dormía sobre la impresora---.
¡Ya estoy harto!

Mientras gritaba, se jalaba el cabello y pateaba la mesa. La gata abrió
uno de sus ojos amarillos y bostezó, estaba acostumbrada a los arrebatos
de Sebastián.

Los últimos seis años, Sebastián había estado trabajando en la primera
computadora cuántica del mundo. Una de verdad, no como esas mediocres
que aparecían y luego se ahogaban con tanta información. Hasta había
pensado el nombre: _La Spukhafte 64_.

---Planck---dijo Sebastián dirigiéndose a la gata con voz más
calmada---. Planck, si logro entender esto, nunca volverás a comer Meow
Mix. Te daré pescado y solo de nivel sashimi.

Planck abrió el otro ojo y se levantó, estirándose y lanzando algunas
bolas de pelo dentro del mecanismo de la impresora (Sebastián tenía que
comprar una cada nueve meses). Entonces abrió su hocico:

---Muy bien--- dijo---, te ayudaré.

Sebastián la miró fijamente. Planck movió la cola.

Finalmente, él habló:

---Ya enloquecí, ¿verdad Planck? Supongo que los dos sabíamos que esto
sucedería, pero nunca pensé que tendría esta alucinación en particular.

---Esta no es una alucinación, Sebastián ---Planck se lamió la pata
derecha y luego se rascó detrás de la oreja---. Nunca me pareció que
valiera la pena hablar, pero estoy cansada de comer la porquería esa que
pones en mi plato que, por cierto, nunca lavas. Aunque no tengo pulgares
oponibles, este es un problema que puedo solucionar.

---¿Sabes cómo resolver la computación cuántica?

---No solo yo. Fluffy, Morris y hasta cualquier vil gato de callejón
podría resolverlo.

---Pero, ¿cómo? ---A Sebastián le daba igual si se había vuelto loco.
Estaba dispuesto a enloquecer a cambio de respuestas.

---¿Eso de nuestras nueve vidas?, ¡es una tontería! Como tú, solo
tenemos una vida. La diferencia es que tenemos acceso a nueve universos.
A veces estamos debajo de la rueda de un coche o cayendo de un tejado,
pero, al mismo tiempo, no lo estamos. Confía en mí. Te enseñaré. Mira,
acércate. Pon tu mano en mi cabeza. Así. Ahora, ráscame detrás de las
orejas. Sí, así, muy bien. Ahora, agárrate fuerte.
