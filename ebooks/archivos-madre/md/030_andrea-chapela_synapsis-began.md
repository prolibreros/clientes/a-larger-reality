# When Synapsis Began

# 15. When Synapsis Began @ignore

## By [Andrea Chapela](997b-semblances.xhtml#a05) {.centrado}

I offer you the following scene. Two people. A concert. A spilled drink
that starts a conversation. A cascade of hormones that makes one of them
say, _Want to come back to my place?_ There, just in that one moment we
already have so many coincidences. Probability is always against us. If
we stop and think about it, we realize that the number of moments that
didn’t happen, mutations that didn’t take place, encounters that didn’t
come to be, is far greater than those that did. For example, did you
know that 8% of our DNA comes from the genetic material of viruses? And
not just one virus. Many. They infected one of our ancestors before we
were human, when we were some kind of proto-mammal. It’s true. I read it
in a scientific article. It said that no one knows what all that genetic
material is for. But they think some of those genes are vital to
synapsis. Maybe our consciousness exists by chance, only because one of
our ancestors got sick with the right virus a hundred million years ago.
That’s what I mean by coincidence. And it’s important to recognize the
role it plays on every level of our lives. Like right now, when you
stumbled upon this piece and I offered you a scene. I shared something
with you. Something happened between us a moment ago, right? It’s still
happening. Right now. Just a chance encounter, and we met. It isn’t an
obvious example, like the one where two people bump into each other at a
concert, go home together, talk about their lives. They spend one, two,
three nights together, maybe even years. It isn’t like the
virus that infects a mammal, leaves behind a couple of genes, and
changes everything. But this meeting---our meeting---could change us,
too. You open a book, find a page randomly, and _bam!_, connection
happens. It’s so fragile, so unexpected. Miraculous. Just two people
sharing a text.
