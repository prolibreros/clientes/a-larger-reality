![](../img/dianita_multiversos-multiverse.jpg)

# _Multiversos_ de [Dianita](997a-semblanzas.xhtml#a45) {.centrado}

Esto está fuera de mi zona de confort porque trabajo exclusivamente con fantasía, así que está siendo interesante observar estos temas y en lugar de hacer ilustraciones puramente científicas, darles un giro. (Me arrepiento un poco de no haber perseguido más la Astronomía como quería desde peque). Ya hice este proyecto algo superpersonal para celebrar que puedo juntar ambas áreas. _Multiversos_ acabó siendo de unas bailarinas jarochas con faldas de galaxias (simulando las posibles galaxias en otros universos con vida como la nuestra). 

# [Dianita](997b-semblances.xhtml#a45), _Multiverse_ {.centrado}

# 43. Multiversos / Multiverse @ignore

This lies outside my comfort zone because I work exclusively with fantasy; as a result, it’s interesting to consider these themes and, instead of making purely scientific illustrations, give them a spin (now I kind of regret not having pursued Astronomy like I wanted to as a kid.) So I made this project something superpersonal to celebrate that I can play in both fields. “Multiverse” ended up being jarocha dancers with galaxy skirts (simulating possible galaxies in other universes with life like ours.)
