# Starlight

# 33. Starlight @ignore

## By [Angela Lujan](997b-semblances.xhtml#a07) {.centrado}

Abigail stands very still as she gazes out the window. She places a hand
on the cold glass as she peers into the night sky. She wishes could see
more than the handful of stars whose light is strong enough to pierce
the light pollution of the nearby city.

The city is moving farther away. Her home is moving farther away. She
sways with the movements of the elevator and keeps her balance by
leaning more weight against the window. For a moment she fears the glass
will crack and she will tumble back down to Earth. It holds strong, cool
against her hand. When she is more confident in her balance, she walks
back to her seat.

The elevator is spacious. Others are seated sporadically, clustered near
the windows. Most of them are alone, like Abigail. Abigail is sure that
they too will miss Earth, but not be missed.

The sterility and the silence up here were what she had craved. But
she’s already begun to need the comforts of her home. She’d wanted to
move away from the noise, the chaos, the false light of the city. To
find a calm steadiness, and finally see the real light of the stars.

Alone, the silence is less appealing. She craves the noise, the
distraction of the city. It juxtaposed her. Revealed her. Here, in
silence, she feels she is just more silence. An extension of her
surroundings. She looks into the blankness between the stars. She knows
those stars used to be closer together, that a force in this universe
pushes all things further and further apart. She imagines a great hand,
swiping galaxies aside like breadcrumbs. She imagines it cradling the
elevator, pulling her away from the city, away from Earth, to another
planet, another universe.

She shudders, and lets the thought go. She glances again toward the
window. Far below, the city is a pinpoint of light. The stars are
already brighter here. The stars are more plentiful here. She sees them
sweeping across the sky. She smiles. She’s found starlight, and her
journey has just begun.
