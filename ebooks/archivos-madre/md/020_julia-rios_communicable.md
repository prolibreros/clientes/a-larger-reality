# Communicable

# 10. Communicable @ignore

## By [Julia Rios](997b-semblances.xhtml#a24) {.centrado}

When they come, we don’t hear or see them. UFO enthusiasts talk about
radio signals and lights in the sky. Scientists analyze a strange set of
data for ages before they work out it’s just a distortion from a
microwave oven at the facility where the satellite dishes are listening.
We never think to look for what actually happens.

The year they come, flu season is particularly bad. The vaccine doesn’t
inoculate us against it because it can’t account for every strain. Many
of us die. We are devastated. We are afraid. We romanticize it. Three
separate period dramas about the 1918 influenza pandemic come out to
critical and popular acclaim. “My Dick Can Heal Ya Girl” by Lil Jabby
tops the pop music charts. It’s about how, through the miraculous powers
of sexual potency, his sperm will cure disease. Lil Jabby, dies of the
flu, which he catches from a groupie.

Some of us survive, but the virus hangs on in our systems. Just when we
think it will go away, it comes back again, stronger. We do not
understand that this suffering is growth. That the virus is information.
That our babies and our babies’ babies will remember.

We do not realize what has happened until generations later, when we
have evolved. When our scientists work out that this was first contact.
That we are colonized. That we are assimilated. That we cannot untangle
_us_ from _them_, that the idea of trying is horrifying. We celebrate
Lil Jabby and the others like him. We mourn them because they never
became us, yet they are always with us. We remember our parents and our
grandparents and the lives of creatures who came so far before us that
they hardly seem like us at all, except, we realize now, they _are_ us.
We are them. By sharing the Earth we are all one macroorganism. And,
too, we are connected to other places, other planets. _We_ are the
microwave oven distortion in the satellite dish. _We_ are the call
coming from inside the house.
