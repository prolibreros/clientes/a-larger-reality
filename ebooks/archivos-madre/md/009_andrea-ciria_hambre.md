# Hambre

# 5. Hambre @ignore

## [Andrea Ciria](997a-semblanzas.xhtml#a06) {.centrado}

La última rama verde del arbusto comenzó a secarse y adoptar el tono
amarillo del resto del árbol. El silencio era total. Las hormigas
muertas no podían devorar los cadáveres de las aves carroñeras caídas,
que a su vez no podían comerse los cuerpos de otros animales que
formaban un tapete infinito de cuerpos inertes. Por primera vez en la
historia, todas las especies de la Tierra, incluido el ser humano,
parecían estar juntas, en una armonía estática y sin vida.

El temerario virus Borax-1 modificó fácilmente la información genética
de las células de los seres vivos para multiplicarse a sí mismo y
engulló hasta la última partícula con vida del planeta. Cuando aquello
comenzó, los científicos del mundo trataron en vano de combatir la
pandemia. Pero no había forma de evitar que las células se convirtieran
en virus, ni vivos, ni muertos, siempre a la espera de un nuevo huésped.
Avezados investigadores aseguraron, con razón, que el Borax-1 era
superior a cualquier otro virus conocido, debido a que podía viajar por
aire y bajo el agua, y convertiría a la Tierra en un sitio yermo. Otros
expertos dudaban de la superioridad del virus porque no solo mutaba con
facilidad para hospedarse en cualquier tipo de célula, sino que era
incapaz de detener su mutación y pronto no encontraría hospederos. En lo
que todos estaban de acuerdo era en que el Borax-1 provenía del espacio.

Seguros de la muerte de la última cucaracha, sin duda las más
resistentes, los creadores del Borax-1 salieron de las profundidades del
mar. Estaban hambrientos. Poco a poco se instalaron en las casas de los
difuntos. Pronto se avistaron pequeñas fogatas por doquier, en las que
quemaban a los cadáveres. Llegó la hora de comer. Los platos de porcelana 
estaban llenos de ceniza. 
