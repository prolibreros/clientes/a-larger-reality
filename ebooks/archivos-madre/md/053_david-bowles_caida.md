# La caída

# 27. La caída @ignore

## [David Bowles](997a-semblanzas.xhtml#a12) {.centrado}

Qhirtlahik observó cómo la estrella se desplomaba hacia su aldea.

El sacerdote había explicado que las estrellas eran los Guardianes de la
Oscuridad, donde la Madre Sol ocultaba su rostro cada noche, avergonzada
ante los pecados de la Gente. Las visiones divinas proclamaban que, de
entre todos sus hijos emplumados, solo a la Gente le había dado alma.

Pero la gente era ingrata.

Qhirtlahik dudaba. La duda era su pecado. Observaba los cielos de noche,
marcando con sus garras los caminos de las estrellas en una amplia
sección de corteza.

Era una danza. Más regular que las mareas, que el pasar de las
estaciones, que el ir y venir de la lluvia y el calor y el frío.

Hace una semana, había notado una nueva luz, ensanchándose más cada
noche, desafiando el compás de la danza.

Ahora, incluso cuando la Madre Sol ensangrentaba el horizonte con sus
garras de amanecer, la estrella no se desvanecía. Era casi del tamaño de
la Hermana Luna.

Qhirtlahik juraba ver llamas en aquel rostro.

Mirando colina abajo hacia su aldea, sintió que sus plumas se agitaban
de miedo. Su lengua palpó el aire, un impulso nervioso.

Como uno de los Conocedores, Qhirtlahik había viajado lejos del conjunto
de pueblos.

Ningún otro ser pensante, ningún otro hablante parecía existir más allá
de esta pequeña península que sobresalía cual colmillo del Mar
Iridiscente.

“Tal vez ---pensó--- debería llevarme a mis amigos y huir.

”Esta estrella nos puede consumir a todos. No quedaría Gente alguna, ni
señal de que hayamos existido. Nuestras casas de bajareque destrozadas.
Reducidas a cenizas las armas de madera y herramientas de hueso. Las
muescas que hacemos en corteza mojada se borrarían para siempre.

”¿Y más allá? ¿Qué sobreviviría? ¿Aquellos monstruos pesados ​​cuyo
plumaje se mofa del nuestro? ¿Los corredores, desesperados por volar?
¿Los pequeños cavadores, tímidos, de sangre caliente?”.

Madre Sol levantó su rostro hacia el cielo.

La estrella voraz la deslumbraba.
