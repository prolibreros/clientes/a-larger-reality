# Remembranzas

# 24. Remembranzas @ignore

## [Ana Delia Carrillo](997a-semblanzas.xhtml#a04) {.centrado}

Cuando ella cierra los ojos, aún puede recordar el cielo pintado de
ocres, naranjas y morados de los ocasos. Aún recuerda el olor fresco de
la tierra mojada después de la lluvia. Y los bosques, y la playa, y su
pequeño huerto al fondo del jardín. Pero no habla de eso. Nadie lo hace.
Ellos lo sabían, y se prepararon justo para ese día. Los tildaron de
locos, de fanáticos, y los ridiculizaron hasta el cansancio. Familiares
y amigos se negaron a evacuar a pesar de que agencias como la NASA
anunciaban la inminente catástrofe. Exageraciones, dijeron. Ustedes y
sus cuentos de destrucción, proclamaron. Su pequeño grupo de
sobrevivientes, poco más de una docena de personas, se resguardó en el
refugio antibombas. ¿De qué les sirve ahora jactarse de que tuvieron
razón? De qué, si el meteorito, aún siendo nueve veces más pequeño que
el de Chicxulub, acabó con la agricultura mundial, ocasionó el incendio
de la mayoría de los bosques del planeta y los tsunamis que devastaron
gran parte de las costas, causando la extinción de miles de especies, y
la muerte de millones de seres humanos. Ella no sabe qué le depara el
futuro. No sabe si hay futuro posible. Solo sabe que si cierra los ojos,
aún recuerda las puestas de sol, el olor a tierra mojada, y su huerto al
fondo del jardín.
