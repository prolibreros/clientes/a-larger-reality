# Scripts

These scripts automate different tasks:

* `build_app`. Builds appbook.
* `convert_json_to_txt`. Converts translations of `web/json/lang.json` to `recursos/relaciones/lang.txt`.
* `convert_txt_to_json`. Converts translations of `recursos/relaciones/lang.txt` to `web/json/lang.json`.
* `customize_ebooks`. Customize ebooks according to this project.
* `develop_ebooks`. Produces EPUB and MOBI in `ebooks/out/` (it also calls to `generate_todo-md` and `customize_ebooks`).
* `generate_todo-md`. Merges all MDs in `/ebooks/archivos-madre/md/` to a single MD in `ebooks/archivos-madre/todo/`.
* `include_book-to-json`. Imports the ebook content in `ebooks/archivos-madre/md/` and `ebooks/archivos-madre/xhtml/` to the appbook JSON file in `web/json/book.json`.
