var debug       = false,
    maintenance = false,
    ready       = false,
    player      = get_save('player'),
    level       = get_save('level'),
    font        = get_save('font'),
    lg          = get_lang(),
    default_hp  = 15,
    max_hp      = 20,
    max_maint   = 5,
    speed_type  = 50,
    speed_show  = 250,
    freeze      = 0,
    lang, skills, worlds, mobs, book, crew, donors, book_text;

// > Prototipos

// Ordena las llaves alfabéticamente; viene de: https://stackoverflow.com/questions/5467129
Object.prototype.sort = function () {
  var u = this,
      o = {};
  Object.keys(this).sort().forEach(function(key) {
    o[key] = u[key];
  });
  return o;
}

// Obtiene los id de los cuartos faltantes
Array.prototype.dif = function(e) {
  var x = [];
  for (var i = 0; i < e; i++) {
    var b = false;
    for (var j = 0; j < this.length; j++) {
      var n = this[j];
      if (n == i) {
        b = true;
      }
    }
    if (!b) {
      x.push(i)
    }
  }
  return x;
}

// Elimina elemento por su valor; viene de: https://stackoverflow.com/questions/3954438
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

// Obtiene el mob a partir de su id
Number.prototype.get_mob = function () {
  var mo1 = lang.mobs.elements[this],
      mo2 = mobs.elements[this],
      key = Object.keys(mo1)[0],
      mob;
  mo1        = mo1[key];
  mo2        = mo2[key];
  mob        = Object.assign({}, mo1, mo2);
  mob['key'] = key;
  mob['id']  = parseInt(this);
  return mob.sort();
}

// Remueve etiquetas de color
String.prototype.rm_colors = function () {
  return this.replace(/<\/*(red|green|blue)>/g, '');
}

String.prototype.at = function (s, g = true) {
  g = g ? 'green' : 'red';
  return this.replace(/@\w+/, '<' + g + '>' + s + '</' + g + '>');
}

// >> Todos los 'get'

// Obtiene elementos salvados
function get_save (e) {
  return  localStorage.getItem(e) ?
            JSON.parse(localStorage.getItem(e)) : null;
}

// Obtiene el lenguaje del sistema
function get_lang () {
  var l = window.navigator.userLanguage ||
          window.navigator.language;

  // Elimina el código del país
  l = l.split('-')[0];

  // Si no es inglés ni español, por defecto se va a inglés
  if (l != 'en' && l != 'es') {
    l = 'en';
  }

  return l;
}

// Detecta si es dispositivo móvil; viene de: https://stackoverflow.com/questions/11381673
function get_device () {
  var check = 'mouseup';
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = 'touchend';})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

// Obtiene el último índice de los bloques
function get_div_last_index () {
  var divs = document.getElementsByClassName('block');

  return parseInt(divs[divs.length - 1].id.split('-')[1]);
}

// Otiene el id del cuarto de inicio
function get_start () {
  for (var i = 0; i < level.map.length; i++) {
    var room = level.map[i];

    if (room.start) {
      return i;
    }
  }
}

// Obtiene el cuarto actual
function get_room () {
  return level.map[get_start()];
}

// Obtiene las opciones
function get_option (pick) {
  var div = document.getElementById('block-' + get_div_last_index()),
      par = div.getElementsByClassName('option'),
      opt = null;

  // Guarda cada opción
  for (var i = 0; i < par.length; i++) {
    var p = par[i],
        k = p.getAttribute('key')
        s = p.getAttribute('skill');

    // Si es para moverse, también guarda la dirección
    if (s == 'go') {
      s = s + ' ' + p.getElementsByTagName('green')[0]
                     .getAttribute('direction');
    }

    // Solo si es la elegida
    if (pick == k) {
      opt = s;
    }
  }

  return opt;
}

// Al obtener HP
function get_hp (i) {

  player.hp += i;

  // Si el HP es menos de uno, se pierde
  if (player.hp < 1) {
    player.hp = default_hp;
    on_fail();
  } else if (player.hp > max_hp) {
    player.hp = max_hp;
    insert_div('☺ ' + lang.misc.rest_max[player.lang]);
  }
}

// Inserta el objecto revelado
function reveal (n, k, ach = true) {
  var el = ach ? 'achievements' : 'objects',
      cl = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});

  insert_div('<a class="object" onclick="print_text(\'' +
    k + '\')">' + '🌈 ' + lang.misc.available[player.lang]
    .replace('@target', '<green><b>' + n + '</b></green>') +
    '</a>');

  // Agrega al inventario, ordena y evita repetidos
  player.inventory[el].push(k);
  player.inventory[el] = player.inventory[el].sort(cl.compare);
  player.inventory[el] = [...new Set(player.inventory[el])];
}

// >> Todos los 'insert'

function insert_back (no = false) {
  var text  = no ? lang.nav.no[player.lang] : lang.nav.back[player.lang];

  insert_div('<b class="back" onclick="back(this, ' + no + ')">' + 
             text + '</b>');
  insert_div_baic(true);
}

// Inserta un párrafo al último bloque
function insert_p (cnt, attr1 = '', id = false) {
  var div   = document.getElementById('block-' + get_div_last_index()),
      attr2 = id ? 'id' : 'class';

  // Añade etiqueta 'p', si el texto no la tiene
  if (!cnt.match(/<(p|h\d|blockquote)/) && cnt != '') {
    cnt = '<p>' + cnt  + '</p>';

    // Añade el atributo, si es que tiene
    if (attr1 != '') {
      cnt = cnt.replace('<p>', '<p ' + attr2 + '="' + attr1 + '">');
    }
  }

  div.innerHTML += cnt;
}

// Inserta un bloque
function insert_div (cnt, prompt = false) {
  var index   = get_div_last_index(),
      section = document.getElementsByTagName('section')[0],
      divs;

  // Añade etiqueta 'p', si el texto no la tiene
  if (!cnt.match(/<(p|h\d|blockquote)>/)) {
    cnt = '<p>' + cnt  + '</p>';
  }

  // Inserta el bloque
  section.innerHTML += '<div class="block" id="block-'
                     + (index + 1) + '">' + cnt + '</div>';

  // Evita que existan más de 10 bloques para evitar problemas de memoria
  divs = document.getElementsByClassName('block');
  if (divs.length > 10) {
    divs[0].parentNode.removeChild(divs[0]);
  }
}

// Inserta el contenido del cuarto
function insert_room (i) {
  var room = level.map[i],
      flag = room.flagged ? '<green>★</green> ' : '<green>☆</green> ',
      mob  = typeof room.mob !== 'undefined' ? room.mob.get_mob(): null,
      por  = typeof room.portal !== 'undefined' ? true : false,
      spd  = 0,
      por_text = '',
      mob_text = '';

  // Si hay mob, prepara el texto
  if (mob) {
    mob_text = lang.mobs[mob.type]
                   .on_appear[player.lang]
                   .at(mob.name[player.lang] + ' [⛨ ' + (Math.abs(mob.damage)) +']', false);
    mob_text = '<p class="threat"><red>💀</red> ' +
               (freeze > 0 ? '<green>[⏳ ' + freeze + ']</green> ' : '') +
               mob_text + '</p>';
  }

  // Si hay portal
  if (por) {
    por_text = '<p><green>⚠ ' +  lang.misc.portal[player.lang] + '</green></p>';
  }

  if (player.hp <= 5) {
    spd += speed_show;
    insert_div('🤕 ' + lang.misc.low_hp[player.lang]);
  }

  // Manda a insertar
  setTimeout(function () {
    insert_div('<p>' + flag + room.name[player.lang] + '<br/>'
                     + room.description[player.lang] + '</p>'
                     + por_text + mob_text);

    // Asigna el comienzo al nuevo cuarto
    for (var i = 0; i < level.map.length; i++) {
      if (i == room.id) {
        level.map[i]['start'] = true;
      } else {
        level.map[i]['start'] = false;
      }
    }

    // Verifica si hay logros
    achievement();

    // Inserta el prompt
    setTimeout(function () {
      insert_div_baic();
    }, speed_show);
  }, spd);
}

// Inserta un bloque de SIAB
function insert_div_bais (cmd, cnt, fn) {

  // Inserta el bloque del prompt y otro vacío
  insert_div('<span class="shell"><green>[root@D-1966]</green><br/>#</span>', true);

  // Empieza a mecanografiar
  type(cmd, cnt, fn);
}

// Inserta un bloque de CIAB  
function insert_div_baic (clean = false) {
  var user  = lang.prompt.user[player.lang];

  turns = '[⚁&nbsp;' + player.turns + '] ';
  world = '[☄&nbsp;' + player.world + '] ';
  flags = '[★&nbsp;' + player.flags + '/' + level.rooms + '] ';
  hp    = '[⛨&nbsp;' + player.hp    + '/' + max_hp + '] ';

  // Inserta el bloque solo con el prompt
  if (!clean) {
    insert_div('<blue>[' + user + '@D-1966]</blue><br/>'
               + '<green>' + world + flags + hp + turns + '</green>', true);
  } else {
    insert_div('<blue>[' + user + '@D-1966]</blue><br/>', true);
  }

  // Muestra las opciones
  insert_options(clean);

  // Espera las órdenes
  ready = true;

  // Guarda el juego
  save_game();
}

// Inserta las opciones
function insert_options (clean = false) {
  var room   = get_room(),
      threat = typeof room.mob !== 'undefined' ? true : false,
      add    = 0;

  function get_id_flag (i) {
    var c = get_room().directions[i],
        f = level.map[c].flagged ? '★' : '☆',
        t = '[' + lang.misc.room[player.lang] + c + ' ' + f + ']';

    return t;
  }

  // Plantilla del texto de la opción
  function print_opt (s, i) {
    insert_p('<a class="option" key="' + i + '" skill="'
            + s + '" onclick="navigate(\'' + i + '\')"> ' 
            + lang.skills[s].name[player.lang] + '</a>');
  }

  // Plantilla del text de la opción 'go'
  function print_dir (s, i) {
    var dirs = Object.keys(room.directions);

    for (var j = 0; j < Object.keys(lang.directions).length; j++) {
      var d = Object.keys(lang.directions)[j];

      // Solo si la dirección es posible
      if (dirs.includes(d)) {
        insert_p('<a class="option" key="' + i + '" skill="'
                + s + '" onclick="navigate(\'' + i + '\')"> '
                + lang.skills[s].name[player.lang] + ' <green direction="'
                + d + '">' + lang.directions[d][player.lang] + ' '
                + get_id_flag(d) + '</green></a>');
      }

      i++;
    }
  }

  if (!clean) {
    // Imprime las habilidades
    for (var i = 0; i < player.skills.length; i++) {
      var skill = player.skills[i];

      // Si no es habilidad de combate o, si lo es, que exista un mob
      if ((!skills[skill].vs) || (skills[skill] && threat)) {
        // Todas menos la habilidad de moverse
        if (skill != 'go') {
          print_opt(skill, i + add);
        // La habilidad de moverse imprime todas las direcciones
        } else {
          print_dir(skill, i);
          add = 3;
        }
      }
    }
  }

  // Imprime final del prompt
  insert_p('<span class="shell">$</span>');
}

// >> Todos los 'build'

// Construye la navegación
function build_nav () {
  var nav   = document.getElementsByTagName('nav')[0],
      inv   = lang.nav.inventory[player.lang],
      menu  = lang.nav.menu[player.lang],
      divs;

  nav.innerHTML = 
    '<div class="btn" id="btn-M"><p>' + menu + '</p></div>' +
    '<div class="btn" id="btn-I"><p>' + inv + '</p></div>' +
    '<div class="btn" id="btn-N"><p>🔍</p></div>' +
    '<div class="btn" id="btn-+"><p>+</p></div>' +
    '<div class="btn" id="btn-−"><p>−</p></div>'

  // Agrega las funcionalidades
  divs = document.getElementsByClassName('btn');
  for (var i = 0; i < divs.length; i++) {
    divs[i].addEventListener(get_device(), function (e) {
      navigate(this.id.substring(4,5));
    });
  }
}

// Construye el mapa para mostrar
function build_map_gui () {
  if (player.world < 30) {
    var rooms = level.map,
        rule  = parseInt(Math.sqrt(level.rooms)),
        curr  = 0,
        row1  = '',
        row2  = '',
        map   = [];

    // Genera el mapa
    for (var i = 0; i < rooms.length; i++) {
      var room = rooms[i],
          dirs = room.directions;

      // Añade los colores
      function add_color (s) {

        s = add_zero(s);

        // Añade clase si es un portal
        function add_portal (t) {
          if (room.portal) {
            return '(' + t + ')';
          } else {
            return '[' + t + ']';
          }

          return t;
        }

        if (room.flagged) {
          s = add_portal(s + '★');
        } else {
          s = add_portal(s + '☆');
        }

        if (room.start) {
          s = '<blue>' + s + '</blue>';
        }

        return s;
      }

      // Añade un cero a cuartos con id menor a 10
      function add_zero (a) {
        return a < 10 ? '0' + a : a;
      }

      // Añade las direcciones
      function add_dir (e = true) {
        var d = e ? 'e' : 's',
            f = e ? '—' : '  |   ',
            n = e ? ' ' : '      ',
            v = e ? room.id + 1    == dirs[d] ? true : false : 
                    room.id + rule == dirs[d] ? true : false;

        // La dirección tiene que existir y ser válida
        if (dirs[d] && v) {
          return f;
        } else {
          return n;
        }
      }

      row2 += add_dir(false);

      // Cuando sigue siendo la misma fila
      if (curr < (rule - 1)) {
        row1 += add_color(i) + add_dir();
        curr++;
      // Cuando cambia de fila
      } else {
        row1 += add_color(i);

        // Añade al mapa
        map.push('<p>' + row1 + '</p>');
        map.push('<p>' + row2 + '</p>');

        // Reseteo
        curr = 0;
        row1 = '';
        row2 = '';
      }

      // Con el último elemento se termina el mapa
      if (i == rooms.length - 1) {

        // Evita poner vínculos al este de más
        map.push('<p>' + row1.replace(/-$/, '') + '</p>');

        // Elimina elementos vacíos
        map = map.filter(function (e) {if (!e.match(/<p>\s*<\/p>/)) {return e}});

        // Evita poner vínculos al sur de más
        if (!map[map.length - 1].match(/\d+/)) {
          map.pop();
        }
      }
    }

    // Añade descripciones
    map = '<div class="mono">' + map.join('') + '<br/>' + '</div>' +
          '<p>' + lang.map.blue[player.lang] + '</p>' +
          '<p>' + lang.map.star[player.lang] + '</p>' +
          '<p>' + lang.map.bracket[player.lang] + '</p>';
  } else {
    map = '<p><red>' + lang.map.denied[player.lang] + '</red></p>';
  }

  // Inserta el mapa
  insert_div(map);
}

// Construye niveles de manera aleatoria
function build_level (w) {
  var terrains;

  insert_p(':: Generating level…');
  level = {}

  // Construlle el mapa
  function build_map () {
    var rule  = parseInt(Math.sqrt(level.rooms)),
        start = Math.floor(Math.random() * level.rooms),
        curr  = 1,
        keys  = 0,
        map   = [],
        del;

    // Añade las direcciones cardinales
    function add_link (curr_id, next_id, east = true) {

      // Se entrecruzan para añadir las 4 direcciones
      if (east) {
        map[curr_id]['directions']['e'] = next_id;
        map[next_id]['directions']['w'] = curr_id;
      } else if (!east) {
        map[curr_id]['directions']['s'] = next_id;
        map[next_id]['directions']['n'] = curr_id;
      }
    }

    // Elimina algunas direcciones cardinales
    function del_link () {
      var room = map[Math.floor(Math.random() * map.length)],
          dirs = room['directions'],
          dir1 = Object.keys(dirs)[Math.floor(Math.random() * Object.keys(dirs).length)],
          next = map[dirs[dir1]],
          dir2 = reverse_dir(dir1);

      // Obtiene la dirección inversa
      function reverse_dir (d) {
        switch (d) {
          case 'n': return 's';
          case 'e': return 'w';
          case 's': return 'n';
          case 'w': return 'e';
        }
      }

      if (Object.keys(room['directions']).length > 1 &&
          Object.keys(next['directions']).length > 1) {
        delete room['directions'][dir1];
        delete next['directions'][dir2];
      } else {
        del_link();
      }
    }

    // Verifica direcciones: no puede haber grupo de cuartos aislados
    function verify_links () {
      var connected = [];

      // Recorre todas las direcciones conectadas
      function walk (i) {
        var room = map[i],
            keys = Object.keys(room['directions']);

        // Añade la dirección en la lista de conectadas
        connected.push(i);

        // Camina a las otras direcciones que aún no están en la lista
        for (var i = 0; i < keys.length; i++) {
          var j = room['directions'][keys[i]];
          if (!connected.includes(j)) {
            walk(j);
          }
        }
      }

      walk(i);

      // Hay cuartos aislados cuando la cantidad de conectados difere a la cantidad total
      if (connected.length != level.rooms) {
        var unconnected = connected.dif(level.rooms),
            selected    = unconnected.slice();

        // Depura para dejar cuartos que servirán como portales
        for (var j = 0; j < selected.length; j++) {
          var u = map[selected[j]];

          for (var k = 0; k < Object.keys(u['directions']).length; k++) {
            var v = u['directions'][Object.keys(u['directions'])[k]];
            selected.remove(v);
          }
        }

        // Establece portales
        for (var k = 0; k < selected.length; k++) {
          var room_id = selected[k];
              next_id = get_portal_id(),
              room    = map[room_id],
              next    = map[next_id];

          // Obtiene el id del cuarto del otro lado del portal
          function get_portal_id () {
            var random = Math.floor(Math.random() * level.rooms),
                choose = true;

            // El candidato no puede ser un cuarto aislado
            for (var x = 0; x < unconnected.length; x++) {
              if (random == unconnected[x]) {
                choose = false;
              }
            }

            // El candidato tiene que tener direcciones disponibles
            if (Object.keys(map[random]['directions']).length == 4) {
              choose = false;
            }

            // Si no se cumplen las condiciones, se busca otro candidato
            if (!choose) {
              return get_portal_id();
            } else {
              return random;
            }
          }

          // Añade portal
          function add_portal (a, b) {
            var dirs = ['n','e','w','s'],
                keys = Object.keys(a['directions']),
                pick;

            a['portal'] = true;

            // Elimina los links ya ocupados
            for (var i = 0; i < dirs.length; i++) {
              for (var j = 0; j < keys.length; j++) {
                if (dirs[i] == keys[j]) {
                  dirs.remove(dirs[i])
                }
              }
            }

            // Elige un link de los disponibles
            pick = dirs[Math.floor(Math.random() * dirs.length)];

            a['directions'][pick] = b.id;

          }

          // Genera el link
          add_portal(room, next);
          add_portal(next, room);
        }
      }
    }

    // Asigna mobs a cada cuarto
    function get_mobs () {
      var mobs   = level.mobs,
          i      = Math.floor(Math.random() * mobs.length),
          mob    = mobs[i];
          name   = Object.keys(mob)[0],
          chance = mob[name].chance;

      // Solo si las probabilidades lo permiten
      if (Math.random() < chance) {
        return mob[name].id;
      } else {
        return null;
      }
    }

    // Crea los cuartos sin direcciones
    for (var i = 0; i < level.rooms; i++) {
      var mobs = get_mobs(),
          r1   = Math.floor(Math.random() * terrains.length),
          t    = terrains[r1];

      function get_name (s) {
        return '<green>[' + lang.misc.room[s] + i + ']</green> ' + t.name[s]
      }

      // Añade los elementos siempre presentes
      map.push({
        'id': i,
        'start': false,
        'flagged': false,
        'name': {'en': get_name('en'), 'es': get_name('es')},
        'description': {'en': t.description.en, 'es': t.description.es},
        'directions': {}
      });

      // Asigna el cuarto de comienzo
      if (i == start) {
        map[i]['start'] = true;
      } else {
        delete map[i]['start'];
      }

      // Añade mobs
      if (mobs != null) {
        map[i]['mob'] = mobs;
      }
    }

    // Añade las direcciones
    for (var i = 0; i < map.length; i++) {
      var lucky = Math.floor(Math.random() * 4);

      // Coloca vecino al este
      if (map[i + 1] && curr < rule) {
        add_link(map[i].id, map[i + 1].id);
        curr++;
      } else {
        curr = 1;
      }

      // Coloca vecino al sur
      if (map[i + rule]) {
        add_link(map[i].id, map[i + rule].id, false);
      }
    }

    // Obtiene la cantidad de direcciones direcciones
    for (var i = 0; i < map.length; i++) {
      keys += (Object.keys(map[i]['directions']).length / 2);
    }

    // Obtiene la cantidad de direcciones a eliminar
    del = parseInt(keys * (Math.floor(Math.random() * 4)) / 10);

    // Elimina direcciones
    for (var i = 0; i < del; i++) {
      del_link();
    }

    // Verifica direcciones
    verify_links();

    // Añade el mapa
    level['map'] = map;

    // Guarda el nivel
    save_game();
  }

  // Escoge mobs
  function pick_mobs (m) {
    ms = [];

    // Si todavía no están asignados
    if (Number.isInteger(m)) {
      for (var i = 0; i < m; i++) {
        var rdm = Math.floor(Math.random() * mobs.elements.length);
        ms.push(mobs.elements[rdm]);
      }
    // Si ya están asignados
    } else {
      for (var i = 0; i < mobs.elements.length; i++) {
        var mob_storaged = mobs.elements[i];

        for (var j = 0; j < m.length; j++) {
          var mob_wanted = m[j];
          if (Object.keys(mob_storaged)[0] == mob_wanted) {
            ms.push(mob_storaged)
          }
        }
      }
    }

    return ms;
  }

  // Une todos los terrenos disponibles
  function merge_terrains () {
    var t = [];

    for (var i = 0; i < lang.worlds.length; i++) {
      var w = lang.worlds[i];

      for (var j = 0; j < w.terrains.length; j++) {
        t.push(w.terrains[j]);
      }
    }
    return t;
  }

  // Genera un intervalo para 'kind_ratio'
  function get_kind_ratio () {
    var rmd = Math.floor(Math.random() * 4) / 10;

    return 0.5 + rmd
  }

  // Genera rangos para 'range_rest' y 'range_damage'
  function get_ranges (w) {

    // Genera números aleatorios entre uno y el número de nivel actual
    function random (w) {
      rdm = Math.floor(Math.random() * w);
      return rdm == 0 ? 1 : rdm
    }

    return [random(w),random(w)].sort();
  }

  // Obtiene elementos en dos idiomas
  function get_langs (s, t = true) {
    if (t) {
      return {'en': texts[s].en, 'es': texts[s].es};
    } else {
      return {'en': lang.misc[s].en, 'es': lang.misc[s].es};
    }
  }

  // Si hay datos del mundo
  if (w <= worlds.data.length) {
    var world = worlds.data[player.world - 1],
        texts = lang.worlds[player.world - 1];

    terrains = texts.terrains;

    level = JSON.parse(JSON.stringify(world));
    level['status'    ] = 'new';
    level['name'      ] = get_langs('name');
    level['mobs'      ] = pick_mobs(level.mobs);
    level['on_new'    ] = get_langs('on_new');
    level['on_return' ] = get_langs('on_return');
    level['on_success'] = get_langs('on_success');
    level['on_fail'   ] = get_langs('on_fail');
  // Si se tiene que crear de manera aleatoria
  } else {
    var mbs   = pick_mobs(5 + (w % 5)),
        kind  = get_kind_ratio(),
        name  = {'en': lang.misc.world.es + ' ' + w, 
                 'es': lang.misc.world.es + ' ' + w};

    terrains = merge_terrains();

    level['status'    ] = 'return';
    level['name'      ] = name;
    level['rooms'     ] = 25;
    level['mobs'      ] = mbs;
    level['bonus'     ] = {};
    level['on_new'    ] = get_langs('on_new', false);
    level['on_return' ] = get_langs('on_return', false);
    level['on_success'] = get_langs('on_success', false);
    level['on_fail'   ] = get_langs('on_fail', false);

    level['bonus']['kind_ratio'  ] = kind;
    level['bonus']['range_rest'  ] = get_ranges(w);
    level['bonus']['range_damage'] = get_ranges(w);
  }

  // Por fin está todo listo para construir el mapa
  build_map();

  // Listo para iniciar el nivel
  init_level();
}

// >> Todos los 'init'

// Inicia el juego
function init () {
  var div = document.getElementById('block-1'),
      ps  = document.getElementsByClassName('file');

  // Si está en mantenimiento
  if (maintenance) {
    var m = document.getElementById('maintenance');
    m.innerHTML = lang.misc.maintenance[player.lang];
  }

  // Sustituye barras de progreso congeladas
  for (var i = 0; i < ps.length; i++) {
    var p = ps[i];

    if (p.innerHTML.match(/\d+/)) {
      p.innerHTML = p.innerHTML.replace(/\[.*?\]/, '[ <green>OK</green> ]');
    }
  }

  insert_p(':: Syncing with corresponding BAIS…');

  // Cambia al idioma correspondiente al nuevo jugador
  if (player.init) {
    player.lang = lg;
    player.hp   = default_hp;
    delete player.init; 
  } else {
    lg = player.lang;
  }

  // Manda a construir o iniciar el nivel
  if (level == null) {
    build_level(player.world);
  } else {
    init_level();
  }

  // Manda a construir la navegación
  build_nav();
}

// Inicia niveles
function init_level () {
  var status = level.status,
      cmd, cnt;

  // Resetea temporizador
  freeze = 0;

  // Solo si no se ha ganado o perdido
  if (level.status != 'success' && level.status != 'fail') {
    insert_p(':: Initializing level…');

    // El contenido depende de si es mantenimiento
    if ((maintenance && player.world > max_maint)) {
      cmd = 'cat on_loop.md';
      cnt = '<br/>' + lang.misc.on_loop[player.lang];
    } else {
      cmd = 'cat on_' + status + '.md';
      cnt = '<br/>' + level['on_' + status][player.lang];
    }

    // Llama la bienvenida del SIAB
    insert_div_bais(cmd, cnt, function () {
      var cmd, cnt;

      cmd = 'ssh ' + lang.prompt.user[player.lang].rm_colors()
                   + '@world.' + player.world 
                   + '.room.'  + get_start();
      cnt = '<p><br/><green>☄ [' + lang.misc.planet[player.lang]
                                 + player.world
                                 + ']</green> '
                                 + level.name[player.lang] + '</p>';

      insert_div_bais(cmd, cnt, function () {
          level.status = 'return';
          save_game();
          insert_room(get_start());
      });
    });
  }
}

// >> Acciones

// Al seleccionar algo del menú
function on_menu (id) {
  var s = debug ? 1 : 3;

  type('cat ' + id + '.txt', '', function () {

    // Si es un nuevo juego
    if (id == 'new') {
      insert_div(lang.menu.new.confirm[player.lang]);
      insert_div('<b class="forward" onclick="reset()">' +
                 lang.nav.yes[player.lang] + '</b>');
      insert_back(true);

    // Si es ir al sitio
    } else if (id == 'site') {
      insert_room(get_start());

    // Si es cambiar el idioma
    } else {
      if (player.lang == 'en') {
        player.lang = 'es';
      } else {
        player.lang = 'en';
      }

      // Guarda y resetea
      save_game();
      countdown(s, false, true);  
    }
  });

}

// Al ganar
function on_success () {
  var s = debug ? 1 : 5;

  level.status = 'success';
  save_game();

  setTimeout(function () {
    insert_div('🎉 ' + lang.misc.all_flags[player.lang]);
  }, speed_show);

  setTimeout(function () {
    var t;

    if (player.world <= lang.worlds.length) {
      t = lang.worlds[player.world - 1].on_success[player.lang];
    } else {
      t = lang.misc.on_success[player.lang];
    }

    insert_div_bais('cat on_success.md',
      '<p><br/>' + t + '</p>',
      function () {
        achievement(true);
        player.infections = []
        player.flags = 0;
        player.world += 1;
        setTimeout(function () {
          countdown(s);
        }, speed_show);
    });
  }, speed_show * 2);
}

// Al perder
function on_fail () {
  var s = debug ? 1 : 5;

  level.status = 'fail';
  save_game();

  setTimeout(function () {
    insert_div('😭 ' + lang.misc.no_hp[player.lang]);
  }, speed_show);

  setTimeout(function () {
    var t;

    if (player.world <= lang.worlds.length) {
      t = lang.worlds[player.world - 1].on_fail[player.lang];
    } else {
      t = lang.misc.on_fail[player.lang];
    }

    insert_div_bais('cat on_fail.md',
      '<p><br/>' + t + '</p>',
      function () {
        player.infections = []
        player.flags = 0;
        setTimeout(function () {
          countdown(s, false);
        }, speed_show);
    });
  }, speed_show * 2);
}

// Al explorar
function on_explore (obj) {
  var key = null,
      name;

  insert_div('👁 [⛨ ' + obj.cost + ' ] '
            + lang.skills.explore.on_explore[player.lang]);

  // Solo si el cuarto no ha sido explorado
  if (!get_room().explored) {

    // Si el inventario de objetos no está vacío
    if (player.inventory.objects.length > 0) {
      var last = player.inventory.objects[player.inventory.objects.length - 1],
          next = 'e' + (parseInt(last.replace('e', '')) + 1);

      // Si el siguiente objeto pertenece al mundo
      if (book[next].world <= player.world) {
        key  = next;
        name = lang.book[key].name[player.lang];
      }
    // Si el inventario de objetos está vacío
    } else {
      key  = 'e1';
      name = lang.book[key].name[player.lang];
    }

    // Si hay objecto, se sortea para ver si prevalece
    if (key != null) {
      if (Math.random() > book[key].luck) {
        key = null;
      }
    }
  }

  // Imprime el resultado
  setTimeout(function () {
    // Si hay llave
    if (key != null) {

      // El cuarto se indica como explorado y se revela el item
      get_room().explored = true;
      reveal(name, key, false);

      // Si el objeto contiene una habilidad
      if (book[key].skill) {

        player.skills.push(book[key].skill);

        // Imprime el mensaje
        insert_p('✅ ' + lang.misc.gained[player.lang]
                .replace('@target', '<green><b>' +
                                    lang.skills[book[key].skill].name[player.lang] +
                                    '</b></green>'));
      }
    // Si no hay llave
    } else {
      insert_div('❌ ' + lang.skills.explore.on_explore_fail[player.lang]);
    }
  }, speed_show);
}

// Al marcar
function on_flag (obj) {

  // Si no hay marca
  if (!get_room().flagged) {
    insert_div('★ [⛨ ' + obj.cost + ' ] '
              + lang.skills.flag.on_flag[player.lang]);

    // Agrega y aumenta el número de marcas
    get_room().flagged = true;
    player.flags++;

  // Si hay marca
  } else {
    insert_div('☆ [⛨ ' + obj.cost + ' ] '
              + lang.skills.flag.on_flag_fail[player.lang]);
  }

  // Si ya se pusieron todoas las margas
  if (player.flags >= level.rooms && player.hp > 1) {
    on_success();
  }
}

// Al descansar
function on_rest (obj) {
  insert_div('🔆 [⛨ ' + obj.cost + ' ] '
            + lang.skills.rest.on_rest[player.lang]);
}

// Al moverse
function on_go (obj, dir) {
  // Evita el ataque de mobs y resetea su congelamiento
  get_room().react = false;
  freeze = 0;

  insert_div('🚀 [⛨ ' + obj.cost + ' ] '
            + lang.skills.go.on_go[player.lang]
                         .at(lang.directions[dir][player.lang]));
}

// Al esterilizar
function on_sterilize (o) {
  var id   = get_room().mob,
      obj  = mobs.elements[id],
      mob  = obj[Object.keys(obj)[0]]
      name = lang.mobs.elements[id][Object.keys(obj)[0]].name[player.lang];

  // Determina si el ataque es eficaz
  if (Math.random() <= mob.chance && mob.type == 'micros') {
    insert_div('✨ [⛨ ' + o.cost + ' ] '
              + lang.skills.sterilize.on_sterilize[player.lang]
                    .replace('@target', '<green>' + name + '</green>'));

    get_room().react = false;
    freeze = skills['sterilize'].duration;
  } else {
    insert_div('❌ [⛨ ' + o.cost + ' ] '
              + lang.skills.sterilize.on_sterilize_fail[player.lang]);
  }
}

// Al paralizar
function on_paralyze (o) {
  var id   = get_room().mob,
      obj  = mobs.elements[id],
      mob  = obj[Object.keys(obj)[0]]
      name = lang.mobs.elements[id][Object.keys(obj)[0]].name[player.lang];

  // Determina si el ataque es eficaz
  if (Math.random() <= mob.chance && mob.type == 'dinos') {
    insert_div('⚡ [⛨ ' + o.cost + ' ] '
              + lang.skills.paralyze.on_paralyze[player.lang]
                    .replace('@target', '<green>' + name + '</green>'));

    get_room().react = false;
    freeze = skills['paralyze'].duration;
  } else {
    insert_div('❌ [⛨ ' + o.cost + ' ] '
              + lang.skills.paralyze.on_paralyze_fail[player.lang]);
  }
}

// Al infectar
function on_infect (o) {
  var id   = get_room().mob,
      obj  = mobs.elements[id],
      mob  = obj[Object.keys(obj)[0]]
      name = lang.mobs.elements[id][Object.keys(obj)[0]].name[player.lang];

  // Determina si el ataque es eficaz
  if (Math.random() <= mob.chance && mob.type == 'dinos') {
    insert_div('💉 [⛨ ' + o.cost + ' ] '
              + lang.skills.infect.on_infect[player.lang]
                    .replace('@target', '<green>' + name + '</green>'));

    get_room().react = false;
    freeze = skills['infect'].duration;
  } else {
    insert_div('❌ [⛨ ' + o.cost + ' ] '
              + lang.skills.infect.on_infect_fail[player.lang]);
  }
}

// Al estar en el turno del mob
function on_mob (next) {
  var spd = 0;

  // Solo si hay mob en el cuarto
  if (typeof get_room().mob !== 'undefined') {
    var id  = get_room().mob,
        obj = mobs.elements[id],
        mob = obj[Object.keys(obj)[0]],
        name = lang.mobs.elements[id][Object.keys(obj)[0]].name[player.lang];

    // Solo si no se está activo
    if (get_room().react && freeze <= 0) {
      spd += speed_show;

      setTimeout(function () {
        var type  = mob.type == 'micros' ? 'micros' : 'dinos',
            emoji = type == 'micros' ? '👾' : '🦖';

        // Imprime el ataque
        insert_div(emoji + ' [⛨ ' + mob.damage + ' ] '
                  + lang.mobs[type].on_attack[player.lang]
                        .replace('@target', '<green>' + name + '</green>'));

        // Aplica el daño
        get_hp(mob.damage);
      }, speed_show);
    } else {
      get_room().react = true;
      freeze--;
    }
  }

  // Avanza al siguiente turno
  setTimeout(function () {
    if (level.status == 'return') {
      insert_room(next);
    }
  }, speed_show + spd);
}

// >> Otros

function reset () {
  type('shutdown -r now', '', function () {
    localStorage.clear();
    location.reload();
  });
}

// Función de regreso
function back (e, no) {
  var backs = document.getElementsByClassName('back'),
      text = no ? 'cancel' : 'back';

  // Elimina los atributos de los back pasados
  for (var i = 0; i < backs.length; i++) {
    backs[i].removeAttribute('onclick');
  }

  // El scroll puede ir de nuevo al fondo
  book_text = null;

  // Elimina los atributos para deshabilitar el 'back'
  e.removeAttribute('onclick');

  // Inserta el cuarto
  type('ruby ' + text + '.rb', '', function () {
    insert_room(get_start());
  });
}

// Imprime la historia
function print_text (id) {
  var cnt;

  book_text = null;

  if (id[0] == 'e') {
    if (id == 'e12') {
      cnt = book[id].data[player.lang]
                    .replace(/<a/g, '<a class="e12"')
                    .replace(/<p/g, '<p id="e12"');
    } else {
      cnt = book[id].data[player.lang];
    }
  } else {
    cnt = book['e52'].data[id][player.lang];
  }

  type('cat ' + id + '.txt', cnt, function () {
    book_text = get_div_last_index();
    insert_back();
  });
}

// Identifica si hay logros
function achievement (w = false) {

  // Iteración de todos los objetos
  for (var i = 0; i < Object.keys(book).length; i++) {
    var key = Object.keys(book)[i],
        obj = book[key],
        nam = lang.book[key].name[player.lang];

    // Si el objeto es un logro
    if (obj.type == 'achievement') {
      var kind     = Object.keys(obj.complete)[0],
          complete = obj.complete[kind],
          success  = false;

      // Identifica si hay logro
      if (kind == 'world') {
        if (w && complete == player.world) {
          success = true;
        }
      } else {
        if (!w && complete == player.turns) {
          success = true;
        }
      }

      // Según si es logro por mundo o turno completado
      if (success) {
        reveal(nam, key);
      }
    }
  }
}

// Cuenta regresiva para iniciar un nivel
function countdown (i, next = true, change_lang = false) {
  var n = next ? 'next' : 'same',
      p, span;

  // Se crea un bloque para la cuenta regresiva si no hay
  if (!document.getElementById('countdown-' + player.world)) {
    insert_div('');

    if (!change_lang) {
      insert_p(':: Preparing for ' + n + ' level in <span></span>…',
               'countdown-' + player.world, true);
    } else {
      insert_p(':: Changing language in <span></span>…',
               'countdown-' + player.world, true);
    }
  }

  // Se imprime la cuenta regresiva
  p    = document.getElementById('countdown-' + player.world);
  span = p.getElementsByTagName('span')[0];
  span.innerHTML = i;

  // Espera un segundo
  setTimeout(function () {
    // Si ya se llegó a cero, se construye el nivel
    if (i <= 0) {
      p.id = '';

      // Según si es para un nuevo nivel o cambio de idioma
      if (!change_lang) {
        build_level(player.world);
      } else {
        location.reload(); 
      }
    // Si no se ha llegado a cero, continúa la cuenta
    } else {
      countdown(i - 1, next, change_lang);
    }
  }, 1000);
}

// Procesa la navegación
function navigate (e) {

  // Aumenta el tamaño de fuente
  function zoom (type) {
    var add  = type == '+' ? true : false,
        size = parseFloat(window.getComputedStyle(document.body, null)
                                .getPropertyValue('font-size'));

    // Si se aumenta, no puede ser mayor a 48px
    if (add) {
      if (size <= 48) {
        size = size + 1;
      }
    // Si se disminuye, no puede ser menor a 10px
    } else {
      if (size >= 10) {
        size = size - 1;
      }
    }

    // Cambia y guarda el tamaño de fuente
    document.body.style.fontSize = size + 'px';
    localStorage.setItem('font', size);
  }

  // Muestra el menú
  function menu () {
    var menu = lang.menu,
        text = '<ol>';

    for (var i = 0; i < Object.keys(menu).length; i++) {
      var element = menu[Object.keys(menu)[i]],
          link;

      // Según el elmento es diferente link
      switch (i) {
        case 0: link = 'on_menu(\'new\')' ; break;
        case 1: link = 'on_menu(\'site\');' +
                       'window.open(\'https://prolibreros.gitlab.io/cc/a-larger-reality/\', \'_blank\')';
                       break;
        case 2: link = 'on_menu(\'lang\')'; break;
      }

      text += '<li><a onclick="' + link + '">' +
              element[player.lang] + '</a></li>';
    }

    text += '</ol>';

    insert_div(text);
    insert_back();
  }

  // Muestra el inventario
  function inventory () {
    var achievements = player.inventory.achievements,
        objects      = player.inventory.objects,
        extras       = player.inventory.extras;

    // Imprime los elementos de cada tipo
    function print_inventory (kind) {
      var header = lang.inventory[kind][player.lang],
          total  = 0,
          items  = '<ol>',
          actual;

      // Determina el tipo
      if (kind == 'achievements') {
        actual = achievements;
      } else if (kind == 'objects') {
        actual = objects;
      } else if (kind == 'extras') {
        actual = extras;
      }

      // Obtiene la cantidad de objetos totales
      for (var i = 0; i < Object.keys(book).length; i++) {
        if (book[Object.keys(book)[i]].type == kind.substring(0, kind.length - 1)) {
          if (Object.keys(book)[i] == 'e52') {
            total += Object.keys(book[Object.keys(book)[i]].data).length;
          } else {
            total++;
          }
        }
      }

      // Encabezado

      insert_div('<h1><b><blue>' + header + ' [' + 
                 (actual == extras ? '' : actual.length + '/') +
                 total + ']</blue></b></h1>');

      // Cuerpo
      for (var i = 0; i < actual.length; i++) {
        var book_obj = book[actual[i]];

        // Si es todo menos los colaboradores
        if (book_obj.subtype != 'crew') {
          var name = lang.book[actual[i]].name[player.lang];
          items += '<li><a onclick="print_text(\'' + 
                   actual[i] + '\')">' + name + '</a></li>';
        // Si son los colaboradores
        } else {
          for (var j = 0; j < Object.keys(book_obj.data).length; j++) {
            var key = Object.keys(book_obj.data)[j],
                col = book_obj.data[key];
            items += '<li><a onclick="print_text(\'' + 
                     key + '\')">' + col.name + '</a></li>';
          }
        }
      }

      insert_div(items + '</ol>');
    }

    // Evita scroll
    book_text = get_div_last_index();

    // Imprime encabezados e inserta un regreso
    print_inventory('achievements');
    print_inventory('objects');
    print_inventory('extras');
    insert_back();
  }

  // Muestra el mapa
  function map () {
    build_map_gui();
    insert_back();
  }

  // Lleva acabo las acciones
  function action (act) {
    var obj = skills[act.split(/\s+/)[0]],
        nxt = get_room().id,
        spd = 0 + speed_show,
        fn  = 'on_' + obj.name,
        dir;

    // Aumenta un turno
    player.turns++;

    // Aumenta el tiempo de espera según la acción
    if (obj.name != 'explore') {
      spd -= speed_show;
    }

    // Llama a la acción = turno del jugador
    if (obj.name == 'go') {
      dir = act.split(/\s+/)[1];
      nxt = get_room().directions[dir];
      window[fn](obj, dir);
    } else {
      window[fn](obj);
    }

    // Calcula el HP
    get_hp(obj.cost);

    // Llama a la reacción = turno del mob
    setTimeout(function () {
      if (level.status == 'return') {
        on_mob(nxt);
      }
    }, spd);
  }

  // La opción de zoom siempre está disponible
  if (e.match(/[+−]/)) {
    zoom(e);
  }

  if (ready) {
    // Si no es acción
    if (e == 'M' || e == 'I' || e == 'N') {

      // El scroll puede ir de nuevo al fondo
      book_text = null;

      if (e == 'M') {
        type('ruby menu.rb', '', menu);
      } else if (e == 'I') {
        type('ruby inventory.rb', '', inventory);
      } else if (e == 'N') {
        type('ruby map.rb', '', map);
      }

    // Si es acción
    } else {
      if (get_option(e) != null) {
        get_room().react = true;
        type(get_option(e), '', function () {
          action(get_option(e));
        });
      }
    }
  }
}

// Guarda el juego
function save_game () {
  localStorage.setItem('player', JSON.stringify(player))
  localStorage.setItem('level', JSON.stringify(level))
}

// Animación de mecanografía
function type (cmd, cnt, fn, f = true) {
  var index = get_div_last_index(),
      div   = document.getElementById('block-' + index),
      span  = div.getElementsByClassName('shell')[0];

  ready = false;

  // Se agrega un espacio la primera vez
  if (f) {
    cmd = ' ' + cmd;
  }

  if (!debug) {

    // Añade el primer caracter del comando
    span.innerHTML += cmd.charAt(0);

    // Si aún quedan caracteres
    if (cmd.length > 0) {

      // Elimina el primer caracter
      cmd = cmd.substring(1, cmd.length);

      // Sigue mecanografiando
      setTimeout(function () {
        type(cmd, cnt, fn, false);
      }, speed_type);
    // Si ya no quedan caracteres
    } else {
      // Añade contenido y corre la función
      insert_p(cnt);
      return fn();
    }
  } else {
    span.innerHTML += cmd;
    insert_p(cnt);
    return fn();
  }
}

// Despliega pantallas de inicio
function intro () {

  // Coloca los elementos básicos para la app
  function basic_build () {
    document.body.innerHTML += '<section><div class="block" id="block-1"></div></section><nav></nav>';

    // Hará que siempre esté el scroll hasta abajo
    document.getElementsByTagName('section')[0]
            .addEventListener('DOMNodeInserted', function (e) {
      var els = this.getElementsByClassName('block');

      if (document.getElementById('block-' + book_text)) {
        document.getElementById('block-' + book_text).scrollIntoView();
      } else {
        this.scrollTo(0, this.scrollHeight);
      }
    }, false );

    // Carga la información
    load();
  }

  // Si no es debug
  if (!debug) {
    var div = document.createElement('div'),
        time;

    // Realiza fade-ins y fade-outs
    function fade () {
      var div = document.getElementById('intro');

      // Si está en fade-in
      if (div.getAttribute('fade-in')) {
        // Si la opacidad es menor a 1, aumenta la opacidad
        if (parseFloat(div.style.opacity) < 1) {
          div.style.opacity = parseFloat(div.style.opacity) + .05;
        // Si la opacidad no es menor a 1, cambia a fade-out
        } else {
          div.removeAttribute('fade-in');
          div.setAttribute('fade-out', true);
        }
      // Si está en fade-out
      } else {
        // Si la opacidad es mayor a 0, disminuye la opacidad
        if (parseFloat(div.style.opacity) > 0) {
          div.style.opacity = parseFloat(div.style.opacity) - .05;
        // Si la opacidad no es mayor a 0, cambia o termina la intro
        } else {
          // Si solo ha pasado el primer logo, cambia al segundo
          if (parseInt(div.getAttribute('screen')) == 1) {  
            div.style.backgroundImage = 'url("img/logo_pt.png")';
            div.removeAttribute('fade-out');
            div.setAttribute('fade-in', true);
            div.setAttribute('screen', 2);
          // Si ya ha pasado el primer logo, termina la intro
          } else {
            clearInterval(timer);
            document.body.removeChild(div);
            basic_build();
          }
        }
      }
    }

    // Propiedades de la intro
    div.id = 'intro';
    div.style.opacity = 0;
    div.style.width = '34vw';
    div.style.height = '80vh';
    div.style.margin = 'auto';
    div.style.maxWidth = '400px'
    div.style.backgroundImage = 'url("img/logo_ct.png")';
    div.style.backgroundPosition = 'center';
    div.style.backgroundSize = 'contain';
    div.style.backgroundRepeat = 'no-repeat';
    div.setAttribute('fade-in', true);
    div.setAttribute('screen', 1);
    document.body.appendChild(div);

    // Arranca animación fade-in y fade-out
    timer = setInterval(fade, 50);
  // Si es debug
  } else {
    basic_build();
  }
}

// Carga los elementos de la partida
function load () {

  var files = [ 'lang.json', 'player.json',
                'skills.json', 'worlds.json',
                'mobs.json', 'book.json',
                'crew.json', 'donors.json']

  // Para obtener la información de los archivos JSON
  function request (url, name) {
    var xhr   = new XMLHttpRequest(),
        div   = document.getElementById('block-1'),
        p     = document.createElement('p'),
        ready = true;

    // Servirá para mostrar la carga
    if (url == 'json/' + files[0]) {
      insert_p(':: Booting system…');
    }

    p.classList.add('file');
    div.appendChild(p);

    // Inicio de la petición
    xhr.open('GET', url);
    xhr.responseType = 'text';
    xhr.send();

    // Cuando está cargando
    xhr.onprogress = function (e) {
      if (e.lengthComputable) {
        p.innerHTML = '[ <blue>' + parseInt((e.loaded / e.total) * 100) + '%</blue> ] ' + name + '.json…';
      } else {
        p.innerHTML = '[ <blue>?</blue> ] ' + name + '.json…';
      }
    };

    // Cuando está listo
    xhr.onreadystatechange = function (e) {
      if (this.readyState == 4 && this.status == 200) {
        var result = JSON.parse(this.responseText);

        p.innerHTML  = '[ <green>OK</green> ] ' + name + '.json…';

        // Torna en variable la información si no existe
        if (window[name] == null || typeof window[name] === 'undefined') {
          window[name] = result;
        }

        // Manda a iniciar el juego si ya se ha cargado todo
        for (var i = 0; i < files.length; i++) {
          var f = files[i].split('.')[0];

          if (window[f] == null || typeof window[f] === 'undefined') {
            ready = false;
          }
        }

        if (ready) {
          init();
        }
      }
    };

    // Cuando hay error
    xhr.onerror = function (e) {
      p.innerHTML = '[ <red>FAIL</red> ] ' + name + '.json…';
    };
  }

  // Llama a cada archivo JSON
  for (var i = 0; i < files.length; i++) {
    var file     = files[i],
        var_name = files[i].split('.')[0];

    request('json/' + file, var_name);
  }
}

// >> Funciones sobre el documento

// Cuando el documento está listo
window.onload = function () {

  // Advertencia del pre-release
  if (maintenance) {
    document.body.innerHTML += '<div id="maintenance"></div>';
  }

  // Aplica el tamaño de fuente, si existe
  if (font) {
    document.body.style.fontSize = font + 'px';
  }

  // Inicia con la introducción
  intro();
}

// Cuando se presiona una tecla
window.onkeyup = function(e) {
  var key = e.keyCode ? e.keyCode : e.which;

  // Llama a la navegación
  switch (key) {
    case 37 : navigate('6'); break;
    case 38 : navigate('3'); break;
    case 39 : navigate('4'); break;
    case 40 : navigate('5'); break;
    case 48 : navigate('0'); break;
    case 49 : navigate('1'); break;
    case 50 : navigate('2'); break;
    case 51 : navigate('3'); break;
    case 52 : navigate('4'); break;
    case 53 : navigate('5'); break;
    case 54 : navigate('6'); break;
    case 55 : navigate('7'); break;
    case 56 : navigate('8'); break;
    case 57 : navigate('9'); break;
    case 69 : navigate('0'); break;
    case 70 : navigate('1'); break;
    case 77 : navigate('M'); break;
    case 73 : navigate('I'); break;
    case 82 : navigate('2'); break;
    case 171: navigate('+'); break;
    case 173: navigate('−'); break;
  }
}

// Previene la habilitación del scroll con las flechas; viene de https://stackoverflow.com/questions/3208021
document.onkeydown = function(evt) {
  var keys = [37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54,
              55, 56, 57, 69, 70, 77, 73, 82, 171, 173];
  evt = evt || window.event;
  for (var i = 0; i < keys.length; i++) {
    if (evt.keyCode >= keys[i] && evt.keyCode <= keys[i]) {
        return false;
    }
  }
};
