// Funciones cuando se carga la página
window.onload = function () {
	var escena   = document.getElementById("scene"),
      title    = document.getElementById("title"),
      subtitle = document.getElementById("subtitle"),
      text     = document.getElementById("text"),
      lang     = (window.navigator.userLanguage ||
                  window.navigator.language).split('-')[0];

  paralaje = new Parallax(scene);

  if (lang == 'es') {
    title.innerHTML    = 'Una realidad más amplia 2.0';
    subtitle.innerHTML = 'Un universo en el que<br/>no nos extinguimos';
    text.innerHTML     = '<i>Un universo en el que no nos extinguimos</i> es una antología híbrida, editada por Libia Brenda, que incluye 39 textos de ficción y de ciencia, 17 ilustraciones y 2 piezas multimedia ¡de 57 colaboradores! Presentada en español e inglés, y con una gran variedad de talentos, esta antología es accesible de forma libre y gratuita (como en “libre expresión”, no como en “cerveza gratis”).<br/><br/>' +
      '<a class="visit" target ="blank" href="https://prolibreros.gitlab.io/cc/a-larger-reality/appbook/www/index.html">Juega</a> ' +
      '<a class="visit" target="blank" href="https://play.google.com/store/apps/details?id=mx.alargerreality.app2019">Google&nbsp;Play</a> ' +
      '<a class="download" href="https://prolibreros.gitlab.io/cc/a-larger-reality/ebooks/out/a-larger-reality.epub">ebook&nbsp;EPUB</a> ' +
      '<a class="download" href="https://prolibreros.gitlab.io/cc/a-larger-reality/ebooks/out/a-larger-reality.mobi">ebook&nbsp;MOBI</a> ' +
      '<a class="visit" target="blank" href="https://perrotuerto.itch.io/a-larger-reality">Ve&nbsp;a&nbsp;itch.io</a> ' +
      '<a class="visit" target="_blank" href="https://gitlab.com/programando-libreros/ludico/a-larger-reality">Ve&nbsp;a&nbsp;GitLab</a>';
  }
};
